import React from "react";
import Routes from "./Routes";
import withProvider from "./HOCS/WithProvider";

const App = () => <Routes />;

export default withProvider(App);
