export const cognitoErrors = error => {
  const { code } = error;
  let message = "";
  let action = false;
  switch (code) {
    case "CodeMismatchException":
      message = "El código no coincide con el enviado.";
      break;
    case "AliasExistsException":
      message = "Una cuenta ya existe con ese correo o email verificado.";
      break;
    case "InvalidPasswordException":
      message =
        "Tu password debe tener almenos 8 caracteres una mayúscula y un número.";
      break;
    case "ExpiredCodeException":
      message = "Codigo invalido, por favor solicite un código nuevo.";
      break;
    case "LimitExceededException":
      message = "Exediste el número máximo de intentos, intenta mas tar";
      break;
    case "UsernameExistsException":
      message = "El nombre de usuario ya existe.";
      break;
    case "NotAuthorizedException":
      message = "Nombre de usuario o contraseña incorrecta.";
      break;
    case "InvalidParameterException":
      message = "El nombre de tu tienda no debe contener espacios.";
      break;
    case "UserNotFoundException":
      message = "El usuario no existe.";
      break;
    case "UserNotConfirmedException":
      message = "Necesitas verificar tu cuenta con el código enviado.";
      action = true;
      break;
    default:
      console.log(error);
  }
  return { message, action };
};
