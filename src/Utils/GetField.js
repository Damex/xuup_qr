import React from "react";
import {
  InputTags,
  Textarea,
  InputNumber,
  Switch,
  Select,
  File,
  ArrayField
} from "../Components/Elements/Common";
import { FormInput } from "../Components/Elements/FormFields";

export const fieldInitialValues = type => {
  switch (type) {
    case "[string]":
    case "[select]":
    case "[file]":
    case "[img]":
      return [];
    case "switch":
      return false;
    default:
      return "";
  }
};

export default field => {
  switch (field.type) {
    case "string":
    case "password":
      return <FormInput {...field} />;
    case "[string]":
      return <InputTags {...field} />;
    case "textarea":
      return <Textarea {...field} />;
    case "integer":
    case "float":
      return <InputNumber {...field} />;
    case "select":
    case "[select]":
      return <Select {...field} />;
    case "switch":
      return <Switch {...field} />;
    case "file":
    case "[file]":
    case "img":
    case "[img]":
      return <File {...field} />;
    case "array":
      return <ArrayField {...field} />;
    default:
      return <div>Elemento inválido</div>;
  }
};
