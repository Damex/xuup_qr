export default obj => {
  return obj && Object.keys(obj).length === 0;
};
