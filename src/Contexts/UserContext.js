import React from "react";

const UserContext = React.createContext({
  signedIn: false,
  profile: {},
  signOut: () => null
});

export default UserContext;
