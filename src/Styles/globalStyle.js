import { injectGlobal } from "styled-components";

injectGlobal`
  @import url('https://fonts.googleapis.com/css?family=Lato:300,400,700|Questrial');

  :root {
  }
  *,
  *::before,
  *::after {
    box-sizing: inherit;    
  }
  #root {
    height: 100%
  }
  html {
    box-sizing: border-box;
    font-size: 87.5%;
  }
  body {
    font-family: "Questrial", sans-serif;
    font-weight: 400;
    line-height: 1.6;
    color: #565656;
    min-height: 100vh;
    padding: 0;
    margin: 0;
  }
`;
