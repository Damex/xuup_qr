export const BP_LARGEST = "75em"; // 1200px
export const BP_LARGE = "68.75em"; // 1100px
export const BP_MEDIUM = "56.25em"; // 900px
export const BP_SMALL = "37.5em"; // 600px
export const BP_SMALLEST = "31.25em"; // 500px

const themeBase = {
  background: "#f6f6f6", // light grey
  backgroundLight: "#fff", // white
  backgroundDark: "f5f6f8", // grey
  textLight: "#a9a8a8", // grey
  text: "#191919" // black
};

export const theme = {
  ...themeBase,
  primary: "#3F8EFC",
  primaryLight: "#61A2FC",
  primaryDark: "#1D2C4D",
  danger: "#FF6363",
  warning: "#FFB363"
};
