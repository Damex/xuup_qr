import React from "react";
import _ from "lodash";
import UserContext from "../Contexts/UserContext";
import { Query } from "react-apollo";
import GetUserData from "../GraphQL/Query/GetUserData";
import StoreForm from "../Components/StoreForm";
import { Grid } from "../../node_modules/@material-ui/core";
import StoreDetail from "../Components/StoreDetail";

const Store = ({ store }) => {
  return (
    <UserContext.Consumer>
      {({ profile }) => {
        if (_.isEmpty(profile)) return null;
        return (
          <Grid container justify="center">
            <Grid item lg={8} xl={6}>
              <Query
                query={GetUserData}
                variables={{
                  PK: profile.username
                }}
              >
                {({ loading, error, data, refetch }) => {
                  if (loading) return "Cargando...";
                  if (error) return <p>Error :(</p>;
                  const {
                    getUserData: { shop = null }
                  } = data;
                  if (!shop) {
                    return (
                      <StoreForm
                        alias={profile.username}
                        email={profile.attributes["email"]}
                        refetch={refetch}
                      />
                    );
                  }
                  return (
                    <StoreDetail shopId={shop} username={profile.username} />
                  );
                }}
              </Query>
            </Grid>
          </Grid>
        );
      }}
    </UserContext.Consumer>
  );
};

export default Store;
