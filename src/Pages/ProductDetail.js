import React from "react";
import _ from "lodash";
import { Query } from "react-apollo";

import GetUserData from "../GraphQL/Query/GetUserData";
import UserContext from "../Contexts/UserContext";
import ProductDetailComponent from "../Components/ProductDetail";

const ProductDetail = ({ match }) => {
  const { params } = match;

  return (
    <UserContext.Consumer>
      {({ profile }) => {
        if (_.isEmpty(profile)) return null;
        return (
          <Query
            query={GetUserData}
            variables={{
              PK: profile.username
            }}
          >
            {({ loading, error, data, refetch }) => {
              if (loading) return "Cargando...";
              if (error) return <p>Error :(</p>;
              const {
                getUserData: { shop = null }
              } = data;

              return (
                <div>
                  <h1>Detalle del producto</h1>
                  <ProductDetailComponent
                    params={params}
                    shopId={shop}
                    username={profile.username}
                  />
                </div>
              );
            }}
          </Query>
        );
      }}
    </UserContext.Consumer>
  );
};

export default ProductDetail;
