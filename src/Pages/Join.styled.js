import styled from "styled-components";
import { Grid, Paper } from "@material-ui/core";
import { BP_MEDIUM } from "../Styles/themes";
import backgroundImage from "../assets/background.jpg";

const GridContainerStyled = styled(Grid)`
  height: 100vh;
  justify-content: flex-end;
  background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0) 0, #ffffff 80%),
    url(${backgroundImage});
  background-size: contain;
  background-size: 100% 700px;
  background-repeat: no-repeat;
  @media only screen and (max-width: ${BP_MEDIUM}) {
    justify-content: center;
    background-size: 100% 400px;
  }
`;

const PaperStyled = styled(Paper)`
  width: 380px;
  margin-right: 5rem;
  @media only screen and (max-width: ${BP_MEDIUM}) {
    margin: 1rem;
  }
  padding: 2rem;
`;

export { PaperStyled, GridContainerStyled };
