import React from "react";
import qs from "query-string";
import SignUp from "../Components/SignUp";
import unauthRoute from "../HOCS/UnauthRoute";
import SignIn from "../Components/SignIn";
import ForgotPassword from "../Components/ForgotPassword";
import VerifyCode from "../Components/VerifyCode";
import { PaperStyled, GridContainerStyled } from "./Join.styled";

const renderSteps = (query, props) => {
  const { step } = query;
  switch (step) {
    case "verify-code":
      return <VerifyCode {...props} />;
    case "forgot-password":
      return <ForgotPassword />;
    case "sign-up":
      return <SignUp />;
    default:
      return <SignIn />;
  }
};

const Join = ({ location }) => {
  const query = qs.parse(location.search);
  return (
    <GridContainerStyled container alignItems="center">
      <PaperStyled elevation={5}>
        {renderSteps(query, location.state)}
      </PaperStyled>
    </GridContainerStyled>
  );
};

export default unauthRoute(Join);
