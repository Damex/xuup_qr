import React from "react";
import _ from "lodash";
import { Link } from "react-router-dom";
import { Query } from "react-apollo";

import GetUserData from "../GraphQL/Query/GetUserData";
import UserContext from "../Contexts/UserContext";
import ListProducts from "../Components/ListProducts";

const Products = () => (
  <UserContext.Consumer>
    {({ profile }) => {
      if (_.isEmpty(profile)) return null;
      return (
        <Query
          query={GetUserData}
          variables={{
            PK: profile.username
          }}
        >
          {({ loading, error, data, refetch }) => {
            if (loading) return "Cargando...";
            if (error) return <p>Error :(</p>;
            const {
              getUserData: { shop = null }
            } = data;

            return (
              <div>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center"
                  }}
                >
                  <h1>Productos</h1>
                  <Link to="/products/new">Nuevo producto</Link>
                </div>
                <ListProducts shopId={shop} username={profile.username} />
              </div>
            );
          }}
        </Query>
      );
    }}
  </UserContext.Consumer>
);

export default Products;
