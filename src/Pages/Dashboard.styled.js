import styled from "styled-components";

const DashBoardContainer = styled.div`
  display: flex;
  flex-grow: 1;
  height: 100vh;
`;

const DashboardContent = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  overflow: auto;
`;

const Content = styled.div`
  flex-grow: 1;
  background-color: #fafafa;
  padding: 2rem;
  overflow-x: none;
  overflow-y: auto;
`;

export { DashBoardContainer, DashboardContent, Content };
