import React from "react";
import { Route, Switch } from "react-router-dom";

import Products from "./Products";
import ProductNew from "./ProductNew";
import ProductDetail from "./ProductDetail";
import Notifications from "./Notifications";
import authRoute from "../HOCS/AuthRoute";

import {
  DashBoardContainer,
  DashboardContent,
  Content
} from "./Dashboard.styled";
import Navigation from "../Components/Navigation";
import Sidebar from "../Components/Sidebar";
import Store from "./Store";
import StoreDetail from "../Components/StoreDetail";

const Dashboard = () => {
  return (
    <DashBoardContainer>
      <Sidebar />
      <DashboardContent>
        <Navigation />
        <Content>
          <Switch>
            <Route exact path="/" render={props => <div>Main dashbaord</div>} />
            <Route exact path="/notifications" component={Notifications} />
            <Route exact path="/store" component={Store} />
            <Route path="/sotore/:id" component={StoreDetail} />
            <Route path="/products/new" component={ProductNew} />
            <Route
              path="/products/p/:product/v/:variant"
              component={ProductDetail}
            />
            <Route path="/products" component={Products} />
          </Switch>
        </Content>
      </DashboardContent>
    </DashBoardContainer>
  );
};

export default authRoute(Dashboard);
