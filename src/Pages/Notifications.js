import React from "react";
import { Button } from "../Components/Elements";

const Notifications = () => (
  <div>
    <Button>Suscribirse a las notificaciones</Button>
  </div>
);

export default Notifications;
