import React from "react";
import NewProduct from "../Components/NewProduct";

const ProductNew = () => (
  <div>
    <h1>Nuevo producto</h1>
    <div>Crear nuevo producto</div>
    <NewProduct />
  </div>
);

export default ProductNew;
