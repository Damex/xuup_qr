import React from "react";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import { ThemeProvider } from "styled-components";
import Join from "./Pages/Join";
import Dashboard from "./Pages/Dashboard";
import { theme } from "./Styles/themes";

const Routes = () => (
  <Router>
    <ThemeProvider theme={theme}>
      <Switch>
        <Route exact path="/join" component={Join} />
        <Route path="/" component={Dashboard} />
      </Switch>
    </ThemeProvider>
  </Router>
);

export default Routes;
