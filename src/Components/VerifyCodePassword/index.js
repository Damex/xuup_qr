import React, { Component } from "react";
import { withRouter, Link } from "react-router-dom";
import _ from "lodash";
import { Auth } from "aws-amplify";
import { validateVerificationForm as validateFields } from "./validations";
import { getField, isEmptyObject } from "../../Utils";
import { cognitoErrors } from "../../Utils/Errors";
import { verificationForm } from "./utils";
import { ButtonStyled } from "../SignUp/SignUp.styled";

class VerifyCode extends Component {
  state = {
    formValues: {},
    errors: {},
    loading: false,
    renderVerifyCode: false
  };
  resendCode = () => {
    const { username } = this.props;
    this.setState({ loading: true }, () => {
      Auth.forgotPassword(username || this.state.formValues.username)
        .then(data => console.log("Se ha reenviado", data))
        .catch(err => this.handleCodeErrors(err));
    });
  };

  isFormCompleted = formFields => {
    const { attributes } = this.props;
    const { formValues, errors } = this.state;
    let hasChange = false;

    if (!formFields.length) {
      return true;
    }

    if (attributes && !isEmptyObject(attributes)) {
      hasChange = _.reduce(
        formFields,
        (sum, n) =>
          sum ||
          (formValues[n.name] && formValues[n.name].trim()) !==
            attributes[n.name],
        false
      );
    } else {
      hasChange = true;
    }

    const requiredStepFields = _.filter(formFields, "required");
    const allHasValue = _.reduce(
      requiredStepFields,
      (sum, n) => sum && !!(formValues[n.name] && formValues[n.name].length),
      true
    );

    return allHasValue && isEmptyObject(errors) && hasChange;
  };

  onSubmit = e => {
    e.preventDefault();
    const { code, newPassword } = this.state.formValues;
    const { username } = this.props;

    this.setState({ loading: true }, () => {
      Auth.forgotPasswordSubmit(username, code, newPassword)
        .then(data => this.props.history.push("/join"))
        .catch(err => this.handleCodeErrors(err));
    });
  };

  handleCodeErrors = error => {
    this.setState({ loading: false });
    const { message } = cognitoErrors(error);
    /// Mostrar mensaje
    console.log(message);
  };

  setFormValue = (value, field) => {
    const { errors } = this.state;
    this.setState(
      prevState => ({
        formValues: {
          ...prevState.formValues,
          ...{ [field]: value }
        }
      }),
      () => {
        const formErrors = validateFields(field, value, errors);
        this.setState({ errors: formErrors });
      }
    );
  };

  setFormErrors = formErrors => {
    this.setState(() => ({ errors: formErrors }));
  };

  render() {
    const { formValues, errors } = this.state;
    const { username } = this.props;
    const isEnabled = this.isFormCompleted(verificationForm.fields);
    return (
      <div>
        <form
          onSubmit={this.onSubmit}
          style={{
            display: "flex",
            flexDirection: "column"
          }}
        >
          {verificationForm.fields.map(field => {
            if (field === "username" && !!username) return null;
            return getField({
              ...field,
              key: field.name,
              name: field.name,
              onChange: this.setFormValue,
              setFormErrors: this.setFormErrors,
              value: formValues[field.name],
              errors
            });
          })}
          <ButtonStyled type="submit" disabled={!isEnabled}>
            Enviar
          </ButtonStyled>
        </form>
        <ButtonStyled dark onClick={this.resendCode}>
          Reenviar código
        </ButtonStyled>
        <Link to="/join">Iniciar sesión</Link>
      </div>
    );
  }
}

export default withRouter(VerifyCode);
