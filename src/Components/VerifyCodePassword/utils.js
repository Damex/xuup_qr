export const verificationForm = {
  title: "Verificar",
  fields: [
    {
      type: "string",
      name: "username",
      label: "Usuario",
      help: "",
      placeholder: "",
      required: true,
      validation: [],
      options: []
    },
    {
      type: "string",
      name: "code",
      label: "Código de verificación",
      help: "Ingresa el código de verificación",
      placeholder: "Código de verificación",
      required: true,
      validation: [],
      options: []
    },
    {
      type: "password",
      name: "newPassword",
      label: "Nueva contraseña",
      help:
        "Tu contraseña debe contener al menos 8 digitos una mayúscula y número",
      placeholder: "Contraseña",
      required: true,
      validation: [],
      options: []
    }
  ]
};
