export const formSchema = {
  title: "Actualiza tu producto",
  fields: [
    {
      type: "string",
      name: "title",
      label: "Variante",
      help: "Nombre de la variante",
      placeholder: "",
      required: false,
      validation: [],
      options: []
    },
    {
      type: "float",
      name: "price",
      label: "Precio de venta",
      help: "Ingresa el precio de venta incluyendo impuestos",
      placeholder: "",
      required: true,
      validation: [],
      options: []
    },
    {
      type: "float",
      name: "taxes",
      label: "Impuestos",
      help: "Ingresa el % de impuestos (0 - 100)",
      placeholder: "",
      required: true,
      validation: [],
      options: []
    },
    {
      type: "textarea",
      name: "description",
      label: "Descripción",
      help: "Descripción de la variante",
      placeholder: "",
      required: false,
      validation: [],
      options: []
    },
    // {
    //   type: "[img]",
    //   name: "picture",
    //   label: "Fotos",
    //   help: "Ingresa fotos del producto",
    //   placeholder: "",
    //   required: false,
    //   validation: [],
    //   options: []
    // },
    {
      type: "string",
      name: "barcode",
      label: "Código de barras",
      help: "Ingresa el código de barras",
      placeholder: "",
      required: false,
      validation: [],
      options: []
    },
    {
      type: "string",
      name: "sku",
      label: "SKU",
      help: "Ingresa el SKU del producto",
      placeholder: "",
      required: false,
      validation: [],
      options: []
    },
    {
      type: "float",
      name: "cost",
      label: "Costo",
      help: "Ingresa el costo",
      placeholder: "",
      required: false,
      validation: [],
      options: []
    },
    {
      type: "float",
      name: "initialInventory",
      label: "Inventario Inicial",
      help: "Ingresa el inventario inicial",
      placeholder: "",
      required: false,
      validation: [],
      options: []
    },
    {
      type: "integer",
      name: "minOrderQty",
      label: "Orden mínima",
      help: "Ingresa la cantidad mínima para pedidos",
      placeholder: "",
      required: false,
      validation: [],
      options: []
    }
  ]
};
