import React from "react";
import Modal from "react-modal";

import { formSchema } from "./utils";
import { StyledModal, ModalHeaderStyled } from "../Elements/Modal.styled";
import { Button } from "../Elements";
import ProductDetailForm from "./ProductDetailForm";

Modal.setAppElement("body");
class ProductDetailModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: false
    };
  }

  openModal = () => {
    this.setState({ modalIsOpen: true });
  };

  closeModal = () => {
    this.setState({ modalIsOpen: false });
  };

  render() {
    return (
      <React.Fragment>
        <Button onClick={this.openModal} buttonlink>
          Modificar
        </Button>
        <StyledModal
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          contentLabel={formSchema.title}
        >
          <ModalHeaderStyled>
            <div>
              <h2>{formSchema.title}</h2>
            </div>
            <Button
              buttonlink
              onClick={this.closeModal}
              style={{ marginLeft: 16, fontSize: "x-large" }}
            >
              <i className="fas fa-times" />
            </Button>
          </ModalHeaderStyled>

          <ProductDetailForm
            product={this.props.product}
            formFields={formSchema.fields}
            onCancel={this.closeModal}
          />
        </StyledModal>
      </React.Fragment>
    );
  }
}

export default ProductDetailModal;
