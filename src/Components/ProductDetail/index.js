import React from "react";
import moment from "moment";
import numeral from "numeral";
import { Query } from "react-apollo";
import QRCode from "qrcode.react";
import html2canvas from "html2canvas";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";

import GetVariant from "../../GraphQL/Query/GetVariant";
import GetPicture from "../Elements/GetPicture";
import logo from "../../assets/logo_circle_plukke.png";
import ProductDetailModal from "./ProductDetailModal";
import { Button } from "../Elements";
import GetShopData from "../../GraphQL/Query/GetShopData";

class ProductDetailComponent extends React.Component {
  render() {
    const {
      params: { product, variant },
      shopId,
      username
    } = this.props;

    return (
      <Query query={GetShopData} variables={{ shopId, username }}>
        {({ loading, error, data }) => {
          if (loading) return "Cargando...";
          if (error) return <p>Error :(</p>;
          const { getShopData } = data;
          const shopData = JSON.parse(getShopData);

          return (
            <Query
              query={GetVariant}
              variables={{ PK: product, SK: variant }}
              fetchPolicy="cache-and-network"
            >
              {({ loading, error, data }) => {
                // console.log(loading, error, data);
                if (loading) return <div>Cargando...</div>;
                if (error) return <div>Error :(</div>;

                const { getVariant: product } = data;
                // console.log(product);

                const qrValue = {
                  ot: "0001",
                  dOp: [
                    { alias: shopData.alias },
                    { cl: shopData.cl },
                    { type: shopData.type },
                    { refn: "" },
                    { refa: shopData.refa },
                    { amount: product.price },
                    { bank: shopData.bank },
                    { country: shopData.country },
                    { currency: shopData.currency },
                    { ic: product.SK } // item SK o si es vacio es un pago a nivel del comercio
                  ]
                };

                return (
                  <Grid container spacing={24}>
                    <Grid item xs={12} style={{ textTransform: "uppercase" }}>
                      <h2 style={{ margin: 0 }}>
                        {product.productData.title} {product.title}
                      </h2>
                      <ProductDetailModal product={product} />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <Paper
                        style={{
                          height: 210,
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                      >
                        {product &&
                          product.picture &&
                          JSON.parse(product.picture).map(picture => (
                            <GetPicture
                              key={picture.fileName}
                              variant={variant}
                              picture={picture}
                            />
                          ))}
                      </Paper>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <Paper
                        style={{
                          height: 210,
                          display: "flex",
                          flexDirection: "column",
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                      >
                        <div
                          id={`qrcodec_${product.productData.title}_${
                            product.title
                          }`}
                          style={{
                            width: 160,
                            height: 160,
                            background: "#fff",
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            position: "relative"
                          }}
                        >
                          <div
                            style={{
                              position: "absolute",
                              top: "50%",
                              bottom: "50%",
                              margin: "auto",
                              background: "#fff",
                              height: 40,
                              width: 40,
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                              borderRadius: 10
                            }}
                          >
                            <img src={logo} alt="Logo" height={40} />
                          </div>

                          <QRCode
                            value={JSON.stringify(qrValue)}
                            size={128}
                            bgColor={"#ffffff"}
                            fgColor={"#000000"}
                            level={"Q"}
                            id={"qrcode" + product.title}
                          />
                        </div>
                        <Button
                          buttonlink
                          onClick={() => {
                            html2canvas(
                              document.getElementById(
                                `qrcodec_${product.productData.title}_${
                                  product.title
                                }`
                              )
                            ).then(function(canvas) {
                              const MIME_TYPE = "image/png";

                              const imgURL = canvas.toDataURL(MIME_TYPE);

                              var dlLink = document.createElement("a");
                              dlLink.download = `qrcode_${
                                product.productData.title
                              }_${product.title}`;
                              dlLink.href = imgURL;
                              dlLink.dataset.downloadurl = [
                                MIME_TYPE,
                                dlLink.download,
                                dlLink.href
                              ].join(":");

                              document.body.appendChild(dlLink);
                              dlLink.click();
                              document.body.removeChild(dlLink);
                            });
                          }}
                        >
                          Descargar QR
                        </Button>
                      </Paper>
                    </Grid>
                    <Grid item xs={12}>
                      <Card>
                        <CardContent>
                          <Grid container spacing={24}>
                            <Grid item xs={12} sm={6}>
                              <div style={{ display: "flex" }}>
                                <div style={{ width: 150, maxWidth: 150 }}>
                                  <b>Descripción</b>
                                </div>
                                {product.description}
                              </div>
                              <div style={{ display: "flex" }}>
                                <div style={{ width: 150, maxWidth: 150 }}>
                                  <b>Código de barras</b>
                                </div>
                                {product.barcode}
                              </div>
                              <div style={{ display: "flex" }}>
                                <div style={{ width: 150, maxWidth: 150 }}>
                                  <b>Disponibilidad</b>
                                </div>
                                {product.isSoldOut ? "Agotado" : "Disponible"}
                              </div>
                              <div style={{ display: "flex" }}>
                                <div style={{ width: 150, maxWidth: 150 }}>
                                  <b>Visibilidad</b>
                                </div>
                                {product.isVisible ? "Visible" : "Oculto"}
                              </div>
                              <div style={{ display: "flex" }}>
                                <div style={{ width: 150, maxWidth: 150 }}>
                                  <b>Mínimo de orden</b>
                                </div>
                                {product.minOrderQty}
                              </div>
                            </Grid>
                            <Grid item xs={12} sm={6}>
                              <div style={{ display: "flex" }}>
                                <div style={{ width: 150, maxWidth: 150 }}>
                                  <b>Precio de venta</b>
                                </div>
                                {numeral(product.price).format("$0,0.00")}
                              </div>
                              <div style={{ display: "flex" }}>
                                <div style={{ width: 150, maxWidth: 150 }}>
                                  <b>Costo</b>
                                </div>
                                {numeral(product.cost).format("$0,0.00")}
                              </div>
                              <div style={{ display: "flex" }}>
                                <div style={{ width: 150, maxWidth: 150 }}>
                                  <b>Impuestos</b>
                                </div>
                                {numeral(product.taxes / 100).format("0.00%")}
                              </div>
                              <div style={{ display: "flex" }}>
                                <div style={{ width: 150, maxWidth: 150 }}>
                                  <b>Creación</b>
                                </div>
                                {moment
                                  .unix(product.createdAt)
                                  .format("DD-MM-YYYY")}
                              </div>
                              <div style={{ display: "flex" }}>
                                <div style={{ width: 150, maxWidth: 150 }}>
                                  <b>Actualización</b>
                                </div>
                                {moment
                                  .unix(product.updatedAt)
                                  .format("DD-MM-YYYY")}
                              </div>

                              <div>
                                <b>SKU</b>
                                {product.sku}
                              </div>
                            </Grid>
                          </Grid>
                        </CardContent>
                      </Card>
                    </Grid>
                  </Grid>
                );
              }}
            </Query>
          );
        }}
      </Query>
    );
  }
}

export default ProductDetailComponent;
