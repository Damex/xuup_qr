import React from "react";
// import { Mutation } from "react-apollo";
import _ from "lodash";

import { FormContainer, FormButtonsGroup } from "../Elements/Form.styled";
import { getField, isEmptyObject } from "../../Utils";
import { Button } from "../Elements";
// import validateFields from "./validations";
// import SetFiscalData from "../../GraphQL/Mutation/SetFiscalData";
// import GetUserData from "../../GraphQL/Query/GetUserData";

class ProductDetailForm extends React.Component {
  state = {
    formValues: {},
    errors: {}
  };

  componentDidMount() {
    const { product } = this.props;
    if (product && !isEmptyObject(product)) {
      this.setState({ formValues: product });
    }
  }

  isFormCompleted = formFields => {
    const { formValues, errors } = this.state;

    if (!formFields.length) {
      return true;
    }

    const requiredStepFields = _.filter(formFields, "required");
    const allHasValue = _.reduce(
      requiredStepFields,
      (sum, n) => sum && !!(formValues[n.name] && formValues[n.name].length),
      true
    );

    return allHasValue && isEmptyObject(errors);
  };

  // Función para establecer valor de un campo
  setFormValue = (value, field) => {
    // const { errors } = this.state;
    this.setState(
      prevState => ({
        formValues: {
          ...prevState.formValues,
          ...{ [field]: value ? value.toUpperCase() : "" }
        }
      }),
      () => {
        // const formErrors = validateFields(field, value, errors);
        // this.setState({ errors: formErrors });
      }
    );
  };

  // Función para forzar establecer errores
  setFormErrors = formErrors => {
    this.setState(() => ({ errors: formErrors }));
  };

  onSubmit = (e, updateVariant, isEnabled) => {
    e.preventDefault();
    const { formValues } = this.state;

    if (!isEnabled) {
      console.warn("¡No se envia formulario!");
      return;
    }

    console.log(formValues);
    // updateVariant({
    //   variables: formValues
    // });
  };

  render() {
    const { formFields, onCancel } = this.props;
    const { formValues, errors } = this.state;
    const isEnabled = this.isFormCompleted(formFields);
    return (
      //   <Mutation
      //     mutation={SetFiscalData}
      //     update={(cache, { data }) => {
      //       const { getUserData } = cache.readQuery({
      //         query: GetUserData,
      //         variables: { SK: "consumer" }
      //       });

      //       cache.writeQuery({
      //         query: GetUserData,
      //         variables: { SK: "consumer" },
      //         data: {
      //           getUserData: {
      //             ...getUserData,
      //             fiscalData: JSON.stringify(
      //               JSON.parse(data.setFiscalData).fiscalData
      //             )
      //           }
      //         }
      //       });
      //     }}
      //   >
      //     {(setFiscalData, { loading, error }) => {
      //       if (loading) return <div>Esperando respuesta...</div>;
      //       if (error) return <div>Error al guardar, intenta nuevamente :(</div>;

      //       return (
      <form onSubmit={e => this.onSubmit(e, 1, isEnabled)}>
        <FormContainer>
          {formFields.map(field =>
            getField({
              ...field,
              key: field.name,
              name: field.name,
              onChange: this.setFormValue,
              setFormErrors: this.setFormErrors,
              value: formValues[field.name],
              errors
            })
          )}
        </FormContainer>

        <FormButtonsGroup>
          <Button type="button" onClick={onCancel}>
            Cancelar
          </Button>
          <Button type="submit" primary disabled={!isEnabled}>
            Guardar
          </Button>
        </FormButtonsGroup>
      </form>
      //       );
      //     }}
      //   </Mutation>
    );
  }
}

export default ProductDetailForm;
