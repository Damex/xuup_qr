import React from "react";
import { Storage } from "aws-amplify";

class GetPicture extends React.Component {
  loadInterval = false;
  state = {
    pictureURL: ""
  };

  componentDidMount() {
    const { picture } = this.props;
    this.loadInterval = setInterval(this.getFile(picture), 500);
  }

  componentWillUnmount() {
    this.loadInterval && clearInterval(this.loadInterval);
    this.loadInterval = false;
  }

  getFile = file => {
    const { variant } = this.props;
    Storage.get(`${variant}/${file.key}`, {
      customPrefix: { public: `${file.prefix}/` },
      bucket: file.bucket,
      region: file.region
    }).then(result => {
      this.loadInterval &&
        this.setState(() => {
          return { pictureURL: result, loading: false };
        });
    });
  };

  render() {
    const { picture } = this.props;
    const { pictureURL } = this.state;
    return (
      pictureURL && (
        <img
          src={pictureURL}
          alt={picture.fileName}
          height={160}
          width={160}
          style={{ objectFit: "cover" }}
        />
      )
    );
  }
}

export default GetPicture;
