import styled from "styled-components";

export const FormContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin: 16px 0;
`;

export const FormButtonsGroup = styled.div`
  text-align: right;
`;
