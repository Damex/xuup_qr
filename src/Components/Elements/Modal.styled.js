import React from "react";
import styled from "styled-components";
import Modal from "react-modal";
import { hex2rgba } from "../../Utils";

function ReactModalAdapter({ className, ...props }) {
  const contentClassName = `${className}__content`;
  const overlayClassName = `${className}__overlay`;
  return (
    <Modal
      portalClassName={className}
      className={contentClassName}
      overlayClassName={overlayClassName}
      {...props}
    />
  );
}

export const StyledModal = styled(ReactModalAdapter)`
  &__overlay {
    position: fixed;
    top: 0px;
    left: 0px;
    right: 0px;
    bottom: 0px;
    background-color: ${props => hex2rgba(props.theme.primaryDark, 0.5)};
    z-index: 9999999;
  }

  &__content {
    position: absolute;
    width: 460px;
    max-height: 50%;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%) !important;
    background: ${props => props.theme.backgroundLight};
    overflow: auto;
    -webkit-overflow-scrolling: touch;
    border-radius: 8px;
    box-shadow: 0 1px 2px 0 rgba(60, 64, 67, 0.3),
      0 1px 3px 1px rgba(60, 64, 67, 0.15);
    outline: none;
    padding: 16px 32px;
    z-index: 10000000;
  }
`;

export const ModalHeaderStyled = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-bottom: 16px;
  border-bottom: 1px solid ${props => hex2rgba(props.theme.textLight, 0.2)};
`;
