import React from "react";
import PropTypes from "prop-types";
import { Input } from "../Input.styled";
import {
  FormInputStyled,
  FormInputLeft,
  FIDanger,
  FIHint
} from "./FormInput.styled";

export default class FormInput extends React.Component {
  static propTypes = {
    value: PropTypes.string,
    onChange: PropTypes.func,
    disabled: PropTypes.bool,
    type: PropTypes.string,
    name: PropTypes.string,
    label: PropTypes.string,
    help: PropTypes.string,
    placeholder: PropTypes.string,
    required: PropTypes.bool,
    validation: PropTypes.array,
    options: PropTypes.array,
    errors: PropTypes.object
  };

  static defaultProps = {
    value: "",
    onChange: () => {},
    disabled: false,
    type: "text",
    name: "input",
    label: "Input",
    required: false,
    autoComplete: "off"
  };

  componentDidUpdate(prevProps) {
    if (this.props.value === prevProps.value) {
      this.ref.setSelectionRange(this.selStart, this.selEnd);
    }
  }

  handleOnChange = e => {
    const { name, value } = e.target;
    const { onChange } = this.props;

    this.selStart = this.ref.selectionStart;
    this.selEnd = this.ref.selectionEnd;
    onChange(value, name);
  };

  handleRenderErrors = () => {
    const { element, errors, name } = this.props;
    if (errors) {
      if (!element) {
        return errors[name] && <FIDanger>{errors[name]}</FIDanger>;
      }
      return (
        errors[`${element}_${name}`] && (
          <FIDanger>{errors[`${element}_${name}`]}</FIDanger>
        )
      );
    }

    return null;
  };

  render() {
    const { props } = this;
    return (
      <FormInputStyled>
        <FormInputLeft>
          <div>
            {props.label} <FIDanger>{props.required && "*"}</FIDanger>
          </div>
          <FIHint>
            {this.handleRenderErrors() || <span>{props.help}</span>}
          </FIHint>
        </FormInputLeft>
        <Input
          name={props.name}
          type={props.type === "string" ? "text" : props.type}
          placeholder={props.placeholder}
          value={props.value}
          onChange={this.handleOnChange}
          disabled={props.disabled}
          innerRef={ref => (this.ref = ref)}
        />
      </FormInputStyled>
    );
  }
}
