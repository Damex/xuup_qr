import styled from "styled-components";
import PhoneInput from "react-phone-number-input";
import { hex2rgba } from "../../../Utils";

export const PhoneInputStyled = styled(PhoneInput)`
    flex: 1;
    
  .react-phone-number-input__row {
    /* This is done to stretch the contents of this component */
    display: flex;
    align-items: center;
  }

  .react-phone-number-input__phone {
    /* The phone number input stretches to fill all empty space */
    flex: 1;

    /* The phone number input should shrink
	   to make room for the extension input */
    min-width: 0;
  }

  .react-phone-number-input__icon {
    width: 20px;
    height: 15px;

    border: 1px solid rgba(0, 0, 0, 0.5);

    box-sizing: content-box;
  }

  .react-phone-number-input__icon--international {
    width: calc(0.93em + 2px);
    height: calc(0.93em + 2px);

    padding-left: 0.155em;
    padding-right: 0.155em;

    border: none;
  }

  .react-phone-number-input__error {
    margin-left: calc(1.24em + 2px + 0.3em + 0.35em + 0.5em);
    margin-top: calc(0.3rem);
    color: #d30f00;
  }

  .react-phone-number-input__icon-image {
    max-width: 100%;
    max-height: 100%;
  }

  .react-phone-number-input__ext-input::-webkit-inner-spin-button,
  .react-phone-number-input__ext-input::-webkit-outer-spin-button {
    margin: 0 !important;
    -webkit-appearance: none !important;
    -moz-appearance: textfield !important;
  }

  .react-phone-number-input__ext-input {
    width: 3em;
  }

  .react-phone-number-input__ext {
    white-space: nowrap;
  }

  .react-phone-number-input__ext,
  .react-phone-number-input__ext-input {
    margin-left: 0.5em;
  }

  .react-phone-number-input__country--native {
    position: relative;
    align-self: stretch;
    display: flex;
    align-items: center;
    margin-right: 0.5em;
  }

  .react-phone-number-input__country-select {
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    z-index: 1;
    border: 0;
    opacity: 0;
    cursor: pointer;
  }

  .react-phone-number-input__country-select-arrow {
    display: block;
    content: "";
    width: 0;
    height: 0;
    margin-bottom: 0.1em;
    margin-top: 0.3em;
    margin-left: 0.3em;
    border-width: 0.35em 0.2em 0 0.2em;
    border-style: solid;
    border-left-color: transparent;
    border-right-color: transparent;
    color: #b8bdc4;
    opacity: 0.7;
    transition: color 0.1s;
  }

  /* Something from stackoverflow. */
  .react-phone-number-input__country-select-divider {
    font-size: 1px;
    background: black;
  }

  .react-phone-number-input__country-select:focus
    + .react-phone-number-input__country-select-arrow {
    color: #03b2cb;
  }

  /* Styling phone number input */
  .react-phone-number-input__input {
    border: 0;
    font-family: "Lato", sans-serif;
    border-radius: 4px;
    background: ${props => props.theme.backgroundLight};
    height: 40px;
    border: 1px solid ${props => hex2rgba(props.theme.textLight, 0.3)};
    padding: 0 8px;
    outline-color: ${props => props.theme.primary};
    :-webkit-autofill {
      box-shadow: 0 0 0 30px white inset;
      -webkit-text-fill-color: ${props => props.theme.primaryDark} !important;
    }

    &:not(:last-child) {
      margin-bottom: 8px;
    }

    ${"" /* height: calc(0.3rem * 6); */}
    ${"" /* outline: none; */}
    ${"" /* border-radius: 0; */}
    ${"" /* padding: 0; */}
    appearance: none;
    ${"" /* border: none; */}
    ${"" /* border-bottom: 1px solid #c5d2e0; */}
    ${"" /* transition: border 0.1s; */}
    ${"" /* font-size: inherit; */}
  }

  .react-phone-number-input__input:focus {
    border-color: #03b2cb;
  }

  .react-phone-number-input__input--disabled {
    cursor: default;
  }

  .react-phone-number-input__input--invalid,
  .react-phone-number-input__input--invalid:focus {
    border-color: #eb2010;
  }

  .react-phone-number-input__input:-webkit-autofill {
    box-shadow: 0 0 0 1000px white inset;
  }

  .react-phone-number-input__country .rrui__select__button {
    border-bottom: none;
  }
`;
