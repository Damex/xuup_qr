import styled, { css } from "styled-components";
import { Input } from "../Input.styled";
import { BP_SMALL } from "../../../Styles/themes";
export const FormInputLeft = styled.div`
  flex: 0 0 160px;
  max-width: 160;
  margin-right: 8px;
`;

export const FormInputStyled = styled.label`
  display: flex;
  align-items: center;

  &:not(:last-child) {
    margin-bottom: 8px;
  }

  ${props =>
    props.vertical &&
    css`
      flex-direction: column;
      align-items: flex-start;

      ${FormInputLeft} {
        flex: 1;
      }

      ${Input} {
        width: 100%;
      }
    `};

  @media only screen and (max-width: ${BP_SMALL}) {
    flex-direction: column;
    align-items: flex-start;

    ${FormInputLeft} {
      flex: 1;
    }

    ${Input} {
      width: 100%;
    }
  }
`;

export const FIDanger = styled.span`
  color: ${props => props.theme.danger};
`;

export const FIHint = styled.div`
  color: ${props => props.theme.textLight};
  font-size: x-small;
`;
