import styled, { css } from "styled-components";
import { hex2rgba } from "../../Utils";

export const SelectStyled = styled.div`
  min-width: 120px;
  width: 100%;
  text-align: left;
  color: ${props => props.theme.text};
  position: relative;
`;

export const SelectContainer = styled.div`
  display: flex;
  align-items: center;
`;

export const SelectContent = styled.div`
  flex: 1;
  user-select: none;
`;

export const SelectIcon = styled.i`
  font-size: 12px;
  margin-right: 4px;
  margin-left: 12px;
`;

export const SelectDropdown = styled.div`
  max-width: 100%;
  width: 100%;
  overflow-x: hidden;
  overflow-y: scroll;
  max-height: 200px;
  position: absolute;
  top: 100%;
  left: 0;
  z-index: 1;
  margin-top: 4px;
  border: 1px solid ${props => hex2rgba(props.theme.textLight, 0.2)};
  padding: 4px 0;
  background: white;
  visibility: hidden;

  ${props =>
    props.selectOpen &&
    css`
      visibility: visible;
    `};
`;

export const SelectMenu = styled.ul`
  outline: none;
  margin: 0;
  padding: 0;
  list-style: none;
`;

export const SelectOption = styled.li.attrs({ role: "option" })`
  color: ${props => props.theme.textLight};
  position: relative;
  display: block;
  padding: 8px 12px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  user-select: none;
  &:not(:last-child) {
    border-bottom: 1px solid ${props => hex2rgba(props.theme.textLight, 0.2)};
  }

  &:hover {
    background: ${props => props.theme.primaryLight};
    color: ${props => props.theme.backgroundLight};
  }
`;
