import styled from "styled-components";
import SelectRC from "rc-select";

const SelectStyled = styled(SelectRC)`
  & .rc-select-selection--single {
    height: 40px;
    padding-top: 5px;
  }
  & .rc-select-arrow {
    top: 5px;
  }
`;

export { SelectStyled };
