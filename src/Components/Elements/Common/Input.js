import React from "react";
import PropTypes from "prop-types";
import { Input as InputStyled } from "../Input.styled";

export default class Input extends React.Component {
  static propTypes = {
    value: PropTypes.string,
    onChange: PropTypes.func,
    disabled: PropTypes.bool,
    order: PropTypes.array,
    type: PropTypes.string,
    name: PropTypes.string,
    label: PropTypes.string,
    help: PropTypes.string,
    placeholder: PropTypes.string,
    required: PropTypes.bool,
    validation: PropTypes.array,
    options: PropTypes.array,
    errors: PropTypes.object,
    fields: PropTypes.array,
    currentStep: PropTypes.number,
    element: PropTypes.string,
    origin: PropTypes.arrayOf(PropTypes.string)
  };

  static defaultProps = {
    value: "",
    onChange: () => {},
    disabled: false,
    type: "text",
    name: "input",
    label: "Input",
    required: false,
    autoComplete: "off"
  };

  handleOnChange = e => {
    const { name, value } = e.target;
    const { onChange, fields, currentStep, origin } = this.props;

    onChange(value, name, fields, currentStep, origin);
  };

  handleRenderErrors = () => {
    const { element, errors, name } = this.props;
    if (errors) {
      if (!element) {
        return (
          errors[name] && <span style={{ color: "red" }}>{errors[name]}</span>
        );
      }
      return (
        errors[`${element}_${name}`] && (
          <span style={{ color: "red" }}>{errors[`${element}_${name}`]}</span>
        )
      );
    }

    return null;
  };

  render() {
    const { props } = this;
    return (
      <label>
        {props.label}
        <InputStyled
          name={props.name}
          type={props.type === "string" ? "text" : props.type}
          placeholder={props.placeholder}
          value={props.value}
          onChange={this.handleOnChange}
          disabled={props.disabled}
        />
        {props.required && "*"}
        {props.help}
        {this.handleRenderErrors()}
      </label>
    );
  }
}
