import React from "react";
import PropTypes from "prop-types";
import uuid from "uuid/v4";
import _ from "lodash";

import FormFields from "../../NewProduct/FormFields";
import { fieldInitialValues } from "../../../Utils/GetField";
import { fieldName } from "../../NewProduct/utils";

export default class ArrayField extends React.Component {
  static propTypes = {
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.array,
      PropTypes.object
    ]),
    onChange: PropTypes.func,
    disabled: PropTypes.bool,
    type: PropTypes.string,
    name: PropTypes.string,
    label: PropTypes.string,
    help: PropTypes.string,
    required: PropTypes.bool,
    options: PropTypes.array,
    errors: PropTypes.object,
    fields: PropTypes.array.isRequired,
    min: PropTypes.number,
    max: PropTypes.number
  };

  static defaultProps = {
    value: "",
    onChange: () => {},
    disabled: false,
    type: "array",
    name: "array",
    label: "Array",
    required: false
  };

  constructor(props) {
    super(props);

    this.state = {
      elements: [],
      fields: props.fields
    };
  }

  componentDidMount() {
    const { combinations, value } = this.props;
    if (value) {
      this.setState(() => ({ elements: value }));
    } else if (combinations && combinations.length) {
      console.log("generate variants");
      let obj = {};

      // generate fields for combination options
      const optfields = Object.keys(combinations[0]).map(e => ({
        type: "string",
        name: e,
        label: e.toUpperCase(),
        placeholder: e,
        origin: ["options"]
      }));

      this.setState(prevState => ({
        fields: [...optfields, ...prevState.fields]
      }));

      combinations.forEach(c => {
        obj = {
          ...obj,
          ...{ [uuid()]: this.setElements(c) }
        };
      });
      this.setState(
        () => ({ elements: obj }),
        () => {
          this.handleOnChange();
        }
      );
    } else {
      this.setState(() => ({ elements: { [uuid()]: this.setElements() } }));
    }
  }

  setElements = opts => {
    const { currentStep } = this.props;
    let element = {};
    const variantTitle = opts && _.join(Object.values(opts), " ");
    this.props.fields.forEach(field => {
      if (field.name === "title") {
        element = {
          ...element,
          ...{ [fieldName(field, currentStep)]: variantTitle }
        };
      } else {
        element = {
          ...element,
          ...{ [fieldName(field, currentStep)]: fieldInitialValues(field.type) }
        };
      }
    });
    if (opts) {
      element = {
        ...element,
        ...{ [`${currentStep}_options`]: opts } //set options to variants
      };
    }
    return element;
  };

  handleOnChange = () => {
    const { onChange, name } = this.props;
    const { elements: value } = this.state;

    onChange(value, name);
  };

  //origin: origen de campo dinámico generado en un paso previo
  setFormValue = (value, field, id, origin) => {
    const { currentStep } = this.props;
    let formattedValue = { [field]: value };
    if (origin) {
      formattedValue = {
        [`${currentStep}_${origin[0]}`]: {
          ...this.state.elements[id][`${currentStep}_${origin[0]}`],
          ...{ [field.split("_")[1]]: value }
        }
      };
    }
    this.setState(
      prevState => ({
        elements: {
          ...prevState.elements,
          ...{ [id]: { ...prevState.elements[id], ...formattedValue } }
        }
      }),
      () => {
        this.handleOnChange();
      }
    );
  };

  handleInsertElement = () => {
    this.setState(
      prevState => ({
        elements: {
          ...prevState.elements,
          ...{ [uuid()]: this.setElements() }
        }
      }),
      () => {
        this.handleOnChange();
      }
    );
  };

  handleDeleteElement = id => {
    this.setState(
      prevState => {
        const obj = prevState.elements;
        delete obj[id];
        return { elements: obj };
      },
      () => {
        const { errors, setFormErrors } = this.props;
        const errorKeys = Object.keys(errors);

        this.handleOnChange();

        if (!errorKeys || !errorKeys.length) {
          return;
        }

        _.forEach(
          _.filter(errorKeys, e => {
            return _.startsWith(e, id);
          }),
          f => delete errors[f]
        );
        setFormErrors(errors);
      }
    );
  };

  render() {
    const { props, state } = this;
    return (
      <div>
        {Object.keys(state.elements).map(element => {
          return (
            <React.Fragment key={element}>
              <FormFields
                {...props}
                fields={state.fields}
                formValues={state.elements[element]}
                setFormValue={(value, field, f, s, origin) =>
                  this.setFormValue(value, field, element, origin)
                }
                element={element}
              />
              <button onClick={() => this.handleDeleteElement(element)}>
                Eliminar
              </button>
            </React.Fragment>
          );
        })}
        <button onClick={this.handleInsertElement}>Agregar elemento</button>
      </div>
    );
  }
}
