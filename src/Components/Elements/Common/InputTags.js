import React from "react";
import PropTypes from "prop-types";
import Select from "rc-select";

export default class InputTags extends React.Component {
  static propTypes = {
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
    onChange: PropTypes.func,
    disabled: PropTypes.bool,
    order: PropTypes.array,
    type: PropTypes.string,
    name: PropTypes.string,
    label: PropTypes.string,
    help: PropTypes.string,
    placeholder: PropTypes.string,
    required: PropTypes.bool,
    validation: PropTypes.array,
    options: PropTypes.array,
    errors: PropTypes.object,
    element: PropTypes.string
  };

  static defaultProps = {
    value: [],
    onChange: () => {},
    disabled: false,
    name: "input",
    label: "Input",
    required: false
  };

  handleOnChange = value => {
    const { onChange, name } = this.props;

    onChange(value, name, {});
  };

  handleRenderErrors = () => {
    const { element, errors, name } = this.props;
    if (errors) {
      if (!element) {
        return (
          errors[name] && <span style={{ color: "red" }}>{errors[name]}</span>
        );
      }
      return (
        errors[`${element}_${name}`] && (
          <span style={{ color: "red" }}>{errors[`${element}_${name}`]}</span>
        )
      );
    }

    return null;
  };

  render() {
    const { props } = this;
    return (
      <label>
        {props.label}
        <Select
          placeholder={props.placeholder}
          tags
          dropdownStyle={{ display: "none" }}
          style={{ width: "100%" }}
          disabled={props.disabled}
          maxTagTextLength={100}
          value={props.value}
          onChange={this.handleOnChange}
          tokenSeparators={[","]}
        />
        {props.required && "*"}
        {props.help}
        {this.handleRenderErrors()}
      </label>
    );
  }
}
