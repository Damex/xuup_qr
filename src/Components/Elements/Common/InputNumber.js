import React from "react";
import PropTypes from "prop-types";
import NumberFormat from "react-number-format";

export default class InputNumber extends React.Component {
  static propTypes = {
    value: PropTypes.string,
    onChange: PropTypes.func,
    disabled: PropTypes.bool,
    order: PropTypes.array,
    type: PropTypes.string,
    name: PropTypes.string,
    label: PropTypes.string,
    help: PropTypes.string,
    placeholder: PropTypes.string,
    required: PropTypes.bool,
    validation: PropTypes.array,
    options: PropTypes.array,
    errors: PropTypes.object
  };

  static defaultProps = {
    value: "",
    onChange: () => {},
    disabled: false,
    name: "input",
    label: "Input",
    required: false
  };

  handleOnChange = ({ value }) => {
    const { onChange, name } = this.props;

    onChange(value, name, {});
  };

  handleRenderErrors = () => {
    const { element, errors, name } = this.props;
    if (errors) {
      if (!element) {
        return (
          errors[name] && <span style={{ color: "red" }}>{errors[name]}</span>
        );
      }
      return (
        errors[`${element}_${name}`] && (
          <span style={{ color: "red" }}>{errors[`${element}_${name}`]}</span>
        )
      );
    }

    return null;
  };

  render() {
    const { props } = this;
    return (
      <label>
        {props.label}
        <NumberFormat
          name={props.name}
          placeholder={props.placeholder}
          value={props.value}
          disabled={props.disabled}
          thousandSeparator={true}
          onValueChange={this.handleOnChange}
          allowNegative
          decimalScale={props.type === "integer" ? 0 : 2}
          fixedDecimalScale
        />
        {props.required && "*"}
        {props.help}
        {this.handleRenderErrors()}
      </label>
    );
  }
}
