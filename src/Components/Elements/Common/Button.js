import React from "react";
import PropTypes from "prop-types";

export default class Button extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired
  };

  static defaultProps = {
    type: "button",
    disabled: false,
    onClick: () => {
      console.log("OnClick");
    }
  };

  render() {
    const { children, disabled, onClick } = this.props;
    return (
      <button disabled={disabled} onClick={onClick}>
        {children}
      </button>
    );
  }
}
