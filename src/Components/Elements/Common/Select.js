import React from "react";
import PropTypes from "prop-types";
import { Option } from "rc-select";
import "rc-select/assets/index.css";
import { SelectStyled } from "./Select.styled";

export default class Select extends React.Component {
  static propTypes = {
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
    onChange: PropTypes.func,
    disabled: PropTypes.bool,
    order: PropTypes.array,
    type: PropTypes.string,
    name: PropTypes.string,
    label: PropTypes.string,
    help: PropTypes.string,
    placeholder: PropTypes.string,
    required: PropTypes.bool,
    validation: PropTypes.array,
    options: PropTypes.array,
    errors: PropTypes.object
  };

  static defaultProps = {
    value: "",
    type: "select",
    onChange: () => {},
    disabled: false,
    name: "select",
    label: "Select",
    required: false,
    options: [],
    multiple: false
  };

  handleOnChange = value => {
    const { onChange, name } = this.props;

    onChange(value, name, {});
  };

  handleRenderErrors = () => {
    const { element, errors, name } = this.props;
    if (errors) {
      if (!element) {
        return (
          errors[name] && <span style={{ color: "red" }}>{errors[name]}</span>
        );
      }
      return (
        errors[`${element}_${name}`] && (
          <span style={{ color: "red" }}>{errors[`${element}_${name}`]}</span>
        )
      );
    }

    return null;
  };

  render() {
    const { props } = this;
    return (
      <label
        style={{
          display: "flex",
          flexDirection: "column",
          paddingTop: "5px",
          paddingBottom: "5px"
        }}
      >
        <span>
          {props.label}{" "}
          <span style={{ fontWeight: "bold", color: "red" }}>
            {props.required && "*"}
          </span>
        </span>
        <SelectStyled
          value={props.value}
          optionLabelProp="children"
          style={{ width: "100%", height: 48 }}
          showSearch={false}
          onChange={this.handleOnChange}
          multiple={props.type === "[select]"}
        >
          <Option value="">Selecciona...</Option>
          {props.options &&
            props.options.map(option => (
              <Option key={option.value} value={option.value}>
                {option.label}
              </Option>
            ))}
        </SelectStyled>
        <span
          style={{
            color: "#a9a8a8",
            fontSize: "x-small"
          }}
        >
          {props.help}
        </span>
        {this.handleRenderErrors()}
      </label>
    );
  }
}
