import React from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";

const SliderStyled = styled.span`
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: 0.4s;
  transition: 0.4s;

  &::before {
    position: absolute;
    content: "";
    height: 26px;
    width: 26px;
    left: 4px;
    bottom: 4px;
    background-color: white;
    -webkit-transition: 0.4s;
    transition: 0.4s;
  }

  ${props =>
    props.round &&
    css`
      border-radius: 34px;

      &::before {
        border-radius: 50%;
      }
    `};
`;

const SwitchStyled = styled.label`
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;

  & > input {
    display: none;

    &::checked ~ ${SliderStyled} {
      background-color: #2196f3;
    }

    &::focus ~ ${SliderStyled} {
      box-shadow: 0 0 1px #2196f3;
    }

    &::checked ~ ${SliderStyled}:before {
      -webkit-transform: translateX(26px);
      -ms-transform: translateX(26px);
      transform: translateX(26px);
    }
  }
`;

export default class Switch extends React.Component {
  static propTypes = {
    value: PropTypes.bool,
    onChange: PropTypes.func,
    disabled: PropTypes.bool,
    type: PropTypes.string,
    name: PropTypes.string,
    label: PropTypes.string,
    help: PropTypes.string,
    placeholder: PropTypes.string,
    required: PropTypes.bool,
    errors: PropTypes.object
  };

  static defaultProps = {
    value: false,
    onChange: () => {},
    disabled: false,
    name: "switch",
    label: "Switch",
    required: false
  };

  handleOnChange = e => {
    const { name, value } = e.target;
    const { onChange } = this.props;

    onChange(value, name, {});
  };

  render() {
    const { props } = this;
    return (
      <div>
        {props.label}
        <SwitchStyled>
          <input
            type="checkbox"
            onChange={this.handleOnChange}
            disabled={props.disabled}
            checked={props.value}
            name={props.name}
          />
          <SliderStyled round />
        </SwitchStyled>
        {props.required && "*"}
        {props.help}
        {props.errors && props.errors[props.name] ? (
          <span style={{ color: "red" }}>{props.errors[props.name]}</span>
        ) : null}
      </div>
    );
  }
}
