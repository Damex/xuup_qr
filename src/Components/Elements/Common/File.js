import React from "react";
import PropTypes from "prop-types";
import Dropzone from "react-dropzone";

export default class File extends React.Component {
  static propTypes = {
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
    onChange: PropTypes.func,
    disabled: PropTypes.bool,
    type: PropTypes.string,
    name: PropTypes.string,
    label: PropTypes.string,
    help: PropTypes.string,
    placeholder: PropTypes.string,
    required: PropTypes.bool,
    validation: PropTypes.array,
    errors: PropTypes.object
  };

  static defaultProps = {
    value: "",
    valueBase64: [],
    onChange: () => {},
    disabled: false,
    type: "file",
    name: "file",
    label: "File",
    required: false
  };

  // Push or replace selected file(s)
  handleSetFiles = files => {
    const { type, value } = this.props;
    if (type === "file" || type === "img") {
      return files;
    }

    return [...value, ...files];
  };

  // Función para incluir el archivo seleccionado al estado
  handleOnChange = files => {
    const { onChange, name, value } = this.props;

    if (value.length && value.length === 6) {
      alert("Máximo 6 archivos están permitidos");
      return;
    }

    onChange(this.handleSetFiles(files), name);
  };

  // Función para mostrar errores en pantalla
  handleRenderErrors = () => {
    const { element, errors, name } = this.props;
    if (errors) {
      if (!element) {
        return (
          errors[name] && <span style={{ color: "red" }}>{errors[name]}</span>
        );
      }
      return (
        errors[`${element}_${name}`] && (
          <span style={{ color: "red" }}>{errors[`${element}_${name}`]}</span>
        )
      );
    }

    return null;
  };

  // Función para quitar archivos de los seleccionados
  handleRemoveFile = idx => {
    const { value, onChange, name } = this.props;

    if (idx > -1) {
      value.splice(idx, 1);
    }

    onChange(value, name);
  };

  render() {
    const { props } = this;
    const accept =
      props.type === "img" || props.type === "[img]"
        ? "image/jpeg,image/jpg,image/png"
        : "application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,.csv,.pdf";
    return (
      <div>
        {props.label}
        <div className="dropzone">
          <Dropzone
            name={props.name}
            placeholder={props.placeholder}
            onDrop={this.handleOnChange}
            disabled={props.disabled}
            accept={accept}
            multiple={false}
          >
            <p>
              Prueba a soltar algunos archivos aquí, o haz clic para seleccionar
              los archivos para cargar.
            </p>
          </Dropzone>
        </div>
        <aside>
          <h2>Archivos seleccionados</h2>
          <ul>
            {props.value && props.value.length
              ? props.value.map((f, i) => (
                  <li key={f.name}>
                    {f.name} - {f.size} bytes{" "}
                    {props.type === "img" || props.type === "[img]" ? (
                      <img
                        src={f.preview}
                        alt={f.name}
                        height={50}
                        width={50}
                        style={{ objectFit: "contain" }}
                      />
                    ) : null}
                    <button onClick={() => this.handleRemoveFile(i)}>
                      &times;
                    </button>
                  </li>
                ))
              : null}
          </ul>
        </aside>
        {props.required && "*"}
        {props.help}
        {this.handleRenderErrors()}
      </div>
    );
  }
}
