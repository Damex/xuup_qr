import styled from "styled-components";
import { hex2rgba } from "../../Utils";

export const Input = styled.input.attrs({ autoComplete: "off" })`
  flex: 1;
  border: 0;
  font-family: "Lato", sans-serif;
  border-radius: 4px;
  background: ${props => props.theme.backgroundLight};
  height: 40px;
  border: 1px solid ${props => hex2rgba(props.theme.textLight, 0.3)};
  padding: 0 8px;
  outline-color: ${props => props.theme.primary};
  :-webkit-autofill {
    box-shadow: 0 0 0 30px white inset;
    -webkit-text-fill-color: ${props => props.theme.primaryDark} !important;
  }

  &:not(:last-child) {
    margin-bottom: 8px;
  }
`;
