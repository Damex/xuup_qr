import styled, { css } from "styled-components";

export const Button = styled.button`
  outline: none;
  font-weight: 300;
  font-size: 1rem;
  border-radius: 4px;
  padding: 0.65rem 2rem;
  background: transparent;
  color: ${props => props.theme.primary};
  border: 2px solid ${props => props.theme.primary};
  cursor: pointer;
  &:not(:last-child) {
    margin-right: 20px;
  }
  &:hover {
    transform: translateY(1px);
    box-shadow: 0 2px 3px rgba(0, 0, 0, 0.15);
  }
  ${props =>
    props.primary &&
    css`
      background: ${props => props.theme.primary};
      color: white;
    `};

  ${props =>
    props.buttonlink &&
    css`
      border: 0;
      padding: 0;
      transition: all 0.3s;
      &:hover {
        color: ${props => props.theme.primaryDark};
        transform: none;
        box-shadow: none;
      }
      &:focus {
        outline: none;
      }
    `};

  ${props =>
    props.default &&
    css`
      color: ${props => props.theme.background};
      border: 2px solid ${props => props.theme.background};
    `};

  ${props =>
    props.disabled &&
    css`
      border: 2px solid ${props => props.theme.background};
      color: ${props => props.theme.backgroundDark};
      background: ${props => props.theme.background};
      cursor: not-allowed;
      &:hover {
        transform: none;
        box-shadow: none;
      }
    `};

  ${props =>
    props.warning &&
    css`
      color: ${props => props.theme.warning};
    `};
`;

export const BigButton = styled(Button)`
  height: 36px;
  font-size: 1rem;
  width: 100%;
  margin: 0;
`;
