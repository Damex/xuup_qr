import styled from "styled-components";
import { AppBar, Toolbar } from "@material-ui/core";

const AppBarStyled = styled(AppBar)`
  && {
    height: 48px;
    display: flex;
    justify-content: flex-end;
    background-color: ${props => props.theme.primary};
  }
`;
const ToolbarStyled = styled(Toolbar)`
  && {
    min-height: 0px;
    display: flex;
    justify-content: flex-end;
  }
`;

export { AppBarStyled, ToolbarStyled };
