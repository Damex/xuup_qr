import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { AppBarStyled, ToolbarStyled } from "./Navigation.styled";
import { IconButton, Icon } from "@material-ui/core";
import UserContext from "../../Contexts/UserContext";
class Navigation extends Component {
  render() {
    const { history } = this.props;
    return (
      <UserContext.Consumer>
        {({ signOut }) => (
          <AppBarStyled position="static">
            <ToolbarStyled>
              <div>
                <IconButton
                  color="inherit"
                  onClick={() => history.push("/notifications")}
                >
                  <Icon>notifications</Icon>
                </IconButton>
                <IconButton
                  color="inherit"
                  onClick={() => history.push("/myaccount")}
                >
                  <Icon>account_circle</Icon>
                </IconButton>
                <IconButton color="inherit" onClick={() => signOut()}>
                  <Icon>exit_to_app</Icon>
                </IconButton>
              </div>
            </ToolbarStyled>
          </AppBarStyled>
        )}
      </UserContext.Consumer>
    );
  }
}

export default withRouter(Navigation);
