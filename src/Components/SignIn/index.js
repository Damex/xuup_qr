import React, { Component, Fragment } from "react";
import { withRouter, Link } from "react-router-dom";
import { Auth } from "aws-amplify";
import qs from "query-string";
import { signInForm } from "./utils";
import { validateSignInForm as validateFields } from "./validations";
import { getField } from "../../Utils";
import { cognitoErrors } from "../../Utils/Errors";
import { ButtonStyled } from "../SignUp/SignUp.styled";

class SignIn extends Component {
  state = {
    formValues: {},
    errors: {},
    loading: false
  };

  onSubmit = e => {
    e.preventDefault();
    const { username, password } = this.state.formValues;
    this.setState({ loading: true }, () => {
      Auth.signIn(username, password)
        .then(user => {
          this.handleSignInResponse(user);
        })
        .catch(err => {
          this.handleSignInErrors(err);
        });
    });
  };
  handleSignInResponse = user => {
    const { history, location } = this.props;
    const from = (location.from && location.from.pathname) || "/";
    history.push(from);
  };

  handleSignInErrors = error => {
    this.setState({ loading: false });
    const { message, action } = cognitoErrors(error);
    if (action) {
      this.setState({ loading: false }, () => {
        this.props.history.push({
          pathname: "/join",
          search: qs.stringify({ step: "verify-code" }),
          state: { username: this.state.formValues.username }
        });
      });
    }
    console.log(message);
  };

  setFormValue = (value, field) => {
    const { errors } = this.state;
    this.setState(
      prevState => ({
        formValues: {
          ...prevState.formValues,
          ...{ [field]: value }
        }
      }),
      () => {
        const formErrors = validateFields(field, value, errors);
        this.setState({ errors: formErrors });
      }
    );
  };

  setFormErrors = formErrors => {
    this.setState(() => ({ errors: formErrors }));
  };

  render() {
    const { formValues, errors } = this.state;

    return (
      <Fragment>
        <h2 style={{ textAlign: "center" }}>Iniciar sesión</h2>
        <form onSubmit={this.onSubmit}>
          {signInForm.fields.map(field =>
            getField({
              ...field,
              key: field.name,
              name: field.name,
              onChange: this.setFormValue,
              setFormErrors: this.setFormErrors,
              value: formValues[field.name],
              errors
            })
          )}
          <ButtonStyled type="submit">Iniciar sesión</ButtonStyled>
        </form>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center"
          }}
        >
          <Link
            to={{
              pathname: "/join",
              search: qs.stringify({ step: "forgot-password" })
            }}
          >
            ¿Has olvidado tu contraseña?
          </Link>
          <span>
            ¿No tienes cuenta?{" "}
            <Link
              to={{
                pathname: "/join",
                search: qs.stringify({ step: "sign-up" })
              }}
            >
              Registrate
            </Link>
          </span>
        </div>
      </Fragment>
    );
  }
}

export default withRouter(SignIn);
