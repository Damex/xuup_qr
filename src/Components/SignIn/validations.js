import _ from "lodash";
import { emailRegex } from "./utils";

export const validateSignInForm = (fieldName, value, errors) => {
  if (fieldName === "email") {
    if (!value || !value.length) {
      _.assign(errors, {
        [fieldName]: "¡Requerido!"
      });
    } else if (value && value.length && !emailRegex.test(value)) {
      _.assign(errors, {
        [fieldName]: "¡Ingresa un email válido!"
      });
    } else {
      delete errors[fieldName];
    }
  }

  return errors;
};
