export const signInForm = {
  title: "Bienvenido a Xuup",
  fields: [
    {
      type: "string",
      name: "username",
      label: "Usuario",
      help: "",
      placeholder: "",
      required: true,
      validation: [],
      options: []
    },
    {
      type: "password",
      name: "password",
      label: "Contraseña",
      help: "",
      placeholder: "Contraseña",
      required: true,
      validation: [],
      options: []
    }
  ]
};

export const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
