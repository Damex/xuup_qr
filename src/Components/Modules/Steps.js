import React from "react";
import PropTypes from "prop-types";
import { Button } from "../Elements/Common";

class Steps extends React.Component {
  static propTypes = {
    steps: PropTypes.array.isRequired,
    isCurrentStepCompleted: PropTypes.func,
    setFormValue: PropTypes.func,
    formValues: PropTypes.object
  };

  state = {
    currentStep: 0
  };

  submit = () => {
    console.log("Submit steps");
    // console.log(this.props.formValues);
  };

  next = () => {
    const currentStep = this.state.currentStep + 1;
    this.setState({ currentStep });
  };

  prev = () => {
    const currentStep = this.state.currentStep - 1;
    this.setState({ currentStep });
  };

  setStep = step => {
    this.setState({ currentStep: step - 1 });
  };

  render() {
    const { props } = this;
    const { currentStep } = this.state;
    const isLastStep = currentStep === props.steps.length - 1;
    const isCompleted = props.isCurrentStepCompleted(
      props.schema[currentStep] && props.schema[currentStep].fields,
      currentStep
    );
    props.getCurrentStep(currentStep);

    return (
      <div>
        {props.steps[currentStep]({
          currentStep,
          stepContent: props.schema[currentStep],
          setFormValue: props.setFormValue,
          setFormErrors: props.setFormErrors,
          formValues: props.formValues,
          errors: props.errors,
          prev: this.prev,
          next: this.next,
          submit: props.handleSubmit,
          setStep: this.setStep,
          isLastStep,
          isStepCompleted: isCompleted,
          defaultButtons: (
            <div>
              <Button onClick={this.prev} disabled={!currentStep}>
                Anterior
              </Button>
              <Button
                onClick={isLastStep ? this.submit : this.next}
                disabled={!isCompleted}
              >
                {isLastStep ? "Guardar y Enviar" : "Siguiente"}
              </Button>
            </div>
          )
        })}
      </div>
    );
  }
}

export default Steps;
