import React from "react";
import { Query } from "react-apollo";
import numeral from "numeral";
import { Link } from "react-router-dom";
import QRCode from "qrcode.react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

import GetShopData from "../../GraphQL/Query/GetShopData";

class ItemsList extends React.Component {
  render() {
    const { items, shopId, username } = this.props;
    return (
      <Query query={GetShopData} variables={{ shopId, username }}>
        {({ loading, error, data }) => {
          if (loading) return "Cargando...";
          if (error) return <p>Error :(</p>;
          const { getShopData } = data;
          const shopData = JSON.parse(getShopData);
          // const qrValue = {
          //   ot: "0001",
          //   dOp: [
          //     { alias: shopData.alias },
          //     { cl: shopData.cl },
          //     { type: shopData.type },
          //     { refn: "" },
          //     { refa: shopData.refa.split(" ")[0] },
          //     { amount: "" },
          //     { bank: shopData.bank },
          //     { country: shopData.country },
          //     { currency: shopData.currency },
          //     { ic: shopData.SK } // item SK o si es vacio es un pago a nivel del comercio
          //   ]
          // };

          return (
            <Table
              style={{
                background: "white",
                boxShadow:
                  "0 1px 2px 0 rgba(60, 64, 67, 0.3), 0 1px 3px 1px rgba(60, 64, 67, 0.15)"
              }}
            >
              <TableHead>
                <TableRow>
                  <TableCell>Producto</TableCell>
                  <TableCell numeric>Precio</TableCell>
                  <TableCell numeric>Inventario</TableCell>
                  <TableCell>QR</TableCell>
                  <TableCell>Detalle</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {items.map(item => {
                  const qrValue = {
                    ot: "0001",
                    dOp: [
                      { alias: shopData.alias },
                      { cl: shopData.cl },
                      { type: shopData.type },
                      { refn: "" },
                      {
                        refa:
                          shopData &&
                          shopData.refa &&
                          shopData.refa &&
                          shopData.refa.split(" ")[0]
                      },
                      { amount: item.price },
                      { bank: shopData.bank },
                      { country: shopData.country },
                      { currency: shopData.currency },
                      { ic: item.SK } // item SK o si es vacio es un pago a nivel del comercio
                    ]
                  };
                  return (
                    <TableRow key={item.SK}>
                      <TableCell
                        component="th"
                        scope="row"
                        style={{ textTransform: "uppercase" }}
                      >
                        {item.productData.title} {item.title} {item.price}
                      </TableCell>
                      <TableCell numeric>
                        {numeral(item.price).format("$0,0.00")}
                      </TableCell>
                      <TableCell numeric>{item.inventory}</TableCell>
                      <TableCell>
                        <QRCode
                          value={JSON.stringify(qrValue)}
                          size={128}
                          bgColor={"#ffffff"}
                          fgColor={"#000000"}
                          level={"Q"}
                        />
                      </TableCell>
                      <TableCell>
                        <Link to={`/products/p/${item.product}/v/${item.SK}`}>
                          Ver detalle
                        </Link>
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          );
        }}
      </Query>
    );
  }
}

export default ItemsList;
