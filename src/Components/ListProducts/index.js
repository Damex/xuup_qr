import React from "react";
import { Query } from "react-apollo";
import ListCatalogue from "../../GraphQL/Query/ListCatalogue";
import ItemsList from "./ItemsList";

class ListProducts extends React.Component {
  render() {
    const { shopId, username } = this.props;
    return (
      <Query
        query={ListCatalogue}
        variables={{ PK: "SHOP-shop1" }}
        fetchPolicy="cache-and-network"
      >
        {({ loading, error, data }) => {
          // console.log(loading, error, data);
          if (loading) return <div>Cargando...</div>;
          if (error) return <div>Error :(</div>;

          const {
            listCatalogue: { items }
          } = data;

          return (
            <ItemsList items={items} shopId={shopId} username={username} />
          );
        }}
      </Query>
    );
  }
}

export default ListProducts;
