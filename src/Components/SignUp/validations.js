import _ from "lodash";
import { emailRegex, phoneValidation } from "./utils";

export const validateProfileForm = (fieldName, value, errors) => {
  if (fieldName === "email") {
    if (!value || !value.length) {
      _.assign(errors, {
        [fieldName]: "¡Requerido!"
      });
    } else if (value && value.length && !emailRegex.test(value)) {
      _.assign(errors, {
        [fieldName]: "¡Ingresa un email válido!"
      });
    } else {
      delete errors[fieldName];
    }
  }

  if (fieldName === "phone_number") {
    if (value && value.length && !phoneValidation(value)) {
      _.assign(errors, {
        [fieldName]: "¡Ingresa un número de celular válido!"
      });
    } else {
      delete errors[fieldName];
    }
  }

  return errors;
};
