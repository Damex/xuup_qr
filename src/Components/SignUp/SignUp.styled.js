import styled from "styled-components";

const ButtonStyled = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 40px;
  margin: 8px 0;
  border: none;
  color: white;
  border-radius: 5px;
  background-color: ${props =>
    props.dark ? props.theme.primaryDark : props.theme.primary};
  background-color: ${props => (props.disabled ? "gray" : null)};
  font-size: 1.2rem;
  box-shadow: 0 3px 5px rgba(0, 0, 0, 0.2);
  &:hover {
    transform: ${props => (props.disabled ? "none" : "translateY(-2px)")};
  }
`;

export { ButtonStyled };
