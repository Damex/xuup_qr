import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import qs from "query-string";
import _ from "lodash";
import { Auth } from "aws-amplify";
import { signUpForm } from "./utils";
import { validateProfileForm as validateFields } from "./validations";
import { getField, isEmptyObject } from "../../Utils";
import { cognitoErrors } from "../../Utils/Errors";
import { ButtonStyled } from "./SignUp.styled";

class SignUp extends Component {
  state = {
    formValues: {},
    errors: {},
    loading: false
  };

  isFormCompleted = formFields => {
    const { attributes } = this.props;
    const { formValues, errors } = this.state;
    let hasChange = false;

    if (!formFields.length) {
      return true;
    }

    if (attributes && !isEmptyObject(attributes)) {
      hasChange = _.reduce(
        formFields,
        (sum, n) =>
          sum ||
          (formValues[n.name] && formValues[n.name].trim()) !==
            attributes[n.name],
        false
      );
    } else {
      hasChange = true;
    }

    const requiredStepFields = _.filter(formFields, "required");
    const allHasValue = _.reduce(
      requiredStepFields,
      (sum, n) => sum && !!(formValues[n.name] && formValues[n.name].length),
      true
    );

    return allHasValue && isEmptyObject(errors) && hasChange;
  };
  onSubmit = e => {
    e.preventDefault();
    const {
      username,
      password,
      name,
      email,
      phone_number
    } = this.state.formValues;
    this.setState({ loading: true }, () => {
      Auth.signUp({
        username,
        password,
        attributes: {
          name,
          email,
          phone_number: phone_number.replace(/\s/g, "")
        }
      })
        .then(data => {
          this.setState({ loading: false }, () => {
            this.props.history.push({
              pathname: "/join",
              search: qs.stringify({ step: "verify-code" }),
              state: { username }
            });
          });
        })
        .catch(err => {
          console.log(err);
          this.handleSignUpErrors(err);
        });
    });
  };

  handleSignUpErrors = error => {
    this.setState({ loading: false });
    const { message } = cognitoErrors(error);
    /// Mostrar mensaje
    console.log(message);
  };

  setFormValue = (value, field) => {
    const { errors } = this.state;
    this.setState(
      prevState => ({
        formValues: {
          ...prevState.formValues,
          ...{ [field]: value }
        }
      }),
      () => {
        const formErrors = validateFields(field, value, errors);
        this.setState({ errors: formErrors });
      }
    );
  };

  setFormErrors = formErrors => {
    this.setState(() => ({ errors: formErrors }));
  };

  render() {
    const { formValues, errors } = this.state;
    const isEnabled = this.isFormCompleted(signUpForm.fields);

    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-evenly",
          height: "100%"
        }}
      >
        <ButtonStyled dark onClick={() => this.props.history.push("/join")}>
          ¿Ya tienes una cuenta?
        </ButtonStyled>
        <form
          onSubmit={this.onSubmit}
          style={{ display: "flex", flexDirection: "column" }}
        >
          {signUpForm.fields.map(field =>
            getField({
              ...field,
              key: field.name,
              name: field.name,
              onChange: this.setFormValue,
              setFormErrors: this.setFormErrors,
              value: formValues[field.name],
              errors
            })
          )}
          <ButtonStyled type="submit" disabled={!isEnabled}>
            Enviar
          </ButtonStyled>
        </form>
      </div>
    );
  }
}

export default withRouter(SignUp);
