import { isValidNumber } from "libphonenumber-js";

export const signUpForm = {
  title: "Bienvenido a Xuup",
  fields: [
    {
      type: "string",
      name: "username",
      label: "Nombre de tu tienda",
      help: "Este sera tu usuario",
      placeholder: "",
      required: true,
      validation: [],
      options: []
    },
    {
      type: "string",
      name: "email",
      label: "Email",
      help: "Ingresa tu correo electrónico principal",
      placeholder: "Ingresa tu correo electrónico",
      required: true,
      validation: [],
      options: []
    },
    {
      type: "string",
      name: "name",
      label: "Nombre completo",
      help: "",
      placeholder: "",
      required: true,
      validation: [],
      options: []
    },
    {
      type: "string",
      name: "phone_number",
      label: "Número de celular",
      help: "Ingresa tu número de celular a 10 dígitos",
      placeholder: "Número de celular",
      required: true,
      validation: [],
      options: []
    },
    {
      type: "password",
      name: "password",
      label: "Contraseña",
      help:
        "Tu contraseña debe contener al menos 8 digitos una mayúscula y número",
      placeholder: "Contraseña",
      required: true,
      validation: [],
      options: []
    }
  ]
};

export const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const phoneValidation = (phone_number, country = "MX") => {
  return isValidNumber(phone_number, country);
};
