import React, { Component } from "react";
import { Query } from "react-apollo";
import QRCode from "qrcode.react";
import html2canvas from "html2canvas";

import logo from "../../assets/logo_circle_plukke.png";
import GetShopData from "../../GraphQL/Query/GetShopData";
import { Button } from "../Elements";
import { Paper } from "@material-ui/core";

class StoreDetail extends Component {
  state = {
    toggleView: "qr"
  };

  render() {
    const { username, shopId } = this.props;
    return (
      <Query query={GetShopData} variables={{ shopId, username }}>
        {({ loading, error, data }) => {
          if (loading) return "Cargando...";
          if (error) return <p>Error :(</p>;
          const { getShopData } = data;
          const shopData = JSON.parse(getShopData);

          const qrValue = {
            ot: "0001",
            dOp: [
              { alias: shopData.alias },
              { cl: shopData.cl },
              { type: shopData.type },
              { refn: "" },
              { refa: shopData.refa.split(" ")[0] },
              { amount: "" },
              { bank: shopData.bank },
              { country: shopData.country },
              { currency: shopData.currency },
              { ic: shopData.SK } // item SK o si es vacio es un pago a nivel del comercio
            ]
          };

          return (
            <div style={{ textAlign: "center" }}>
              <Button
                onClick={() =>
                  this.setState(prevState => ({
                    toggleView: prevState.toggleView === "qr" ? "detail" : "qr"
                  }))
                }
                style={{ marginBottom: 16, marginRight: 0 }}
              >
                {this.state.toggleView === "qr"
                  ? "Ver datos del comercio"
                  : "Ver código QR"}
              </Button>
              {this.state.toggleView === "qr" ? (
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    flexDirection: "column"
                  }}
                >
                  <div
                    id={`qrcodec_${shopData.SK}`}
                    style={{
                      width: 200,
                      height: 200,
                      background: "#fff",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                      position: "relative"
                    }}
                  >
                    <div
                      style={{
                        position: "absolute",
                        top: "50%",
                        bottom: "50%",
                        margin: "auto",
                        background: "#fff",
                        height: 40,
                        width: 40,
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        borderRadius: 10
                      }}
                    >
                      <img src={logo} alt="Logo" height={40} />
                    </div>

                    <QRCode
                      value={JSON.stringify(qrValue)}
                      size={180}
                      bgColor={"#ffffff"}
                      fgColor={"#000000"}
                      level={"Q"}
                      id={"qrcode" + shopData.SK}
                    />
                  </div>
                  <Button
                    buttonlink
                    onClick={() => {
                      html2canvas(
                        document.getElementById(`qrcodec_${shopData.SK}`)
                      ).then(function(canvas) {
                        const MIME_TYPE = "image/png";

                        const imgURL = canvas.toDataURL(MIME_TYPE);

                        var dlLink = document.createElement("a");
                        dlLink.download = `qrcode_${shopData.SK}`;
                        dlLink.href = imgURL;
                        dlLink.dataset.downloadurl = [
                          MIME_TYPE,
                          dlLink.download,
                          dlLink.href
                        ].join(":");

                        document.body.appendChild(dlLink);
                        dlLink.click();
                        document.body.removeChild(dlLink);
                      });
                    }}
                  >
                    Descargar QR
                  </Button>
                </div>
              ) : (
                <div>
                  <h3>Detalle del comercio</h3>
                  <Paper
                    style={{
                      padding: 16,
                      display: "flex",
                      alignItems: "flex-start",
                      flexDirection: "column",
                      textAlign: "left"
                    }}
                  >
                    <div>
                      <b>Comercio</b> {shopData.alias}
                    </div>
                    <div>
                      <b>Código del banco</b> {shopData.bank}
                    </div>
                    <div>
                      <b>Tipo</b> {shopData.type}
                    </div>
                    <div>
                      <b>Clabe o Número de tarjeta</b> {shopData.cl}
                    </div>
                  </Paper>
                </div>
              )}
            </div>
          );
        }}
      </Query>
    );
  }
}

export default StoreDetail;
