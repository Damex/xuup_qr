import React from "react";
import styled from "styled-components";
import { ListItem, ListItemText, Icon } from "@material-ui/core";

const IconStyled = styled(Icon)`
  && {
    color: ${props =>
      props.selected ? props.theme.primary : "rgba(255, 255, 255, 0.7)"};
  }
`;

const ListItemTextStyled = styled(({ selected, ...other }) => (
  <ListItemText classes={{ primary: "primary" }} {...other} />
))`
  && {
    padding-left: 0;
  }
  & .primary {
    color: ${props =>
      props.selected ? props.theme.primary : "rgba(255, 255, 255, 0.7)"};
    font-weight: bold;
    font-family: Questrial;
  }
`;

const ListItemStyled = styled(ListItem)`
  && {
    padding: 15px;
    background-color: ${props => (props.selected ? "#f6f6f6" : "transparent")};
    &:hover {
      background-color: rgba(64, 72, 84, 0.3);
      ${IconStyled} {
        color: ${props => (props.selected ? props.theme.primary : "white")};
      }
      ${ListItemTextStyled} {
        & .primary {
          color: ${props => (props.selected ? props.theme.primary : "white")};
        }
      }
    }
  }
`;

export { ListItemStyled, ListItemTextStyled, IconStyled };
