import React from "react";
import styled, { css } from "styled-components";
import { Drawer, List } from "@material-ui/core";
import { ChevronLeft } from "@material-ui/icons";

const DrawerStyled = styled(({ open, ...other }) => (
  <Drawer classes={{ paper: "drawerPaper" }} {...other} />
))`
  & .drawerPaper {
    background-color: #262f3d;
    position: relative;
    border-right: 1px solid #404854;
    ${({ open, theme }) => {
      return open
        ? css`
            white-space: nowrap;
            width: 240px;
            transition: ${theme.transitions.create("width", {
              easing: theme.transitions.easing.sharp,
              duration: theme.transitions.duration.enteringScreen
            })};
          `
        : css`
            overflow-x: hidden;
            transition: ${theme.transitions.create("width", {
              easing: theme.transitions.easing.sharp,
              duration: theme.transitions.duration.leavingScreen
            })};
            width: ${theme.spacing.unit * 7}px;
            [${theme.breakpoints.up("sm")}]: {
              width: ${theme.spacing.unit * 9}px
            }
          `;
    }}
`;

const DrawerContainer = styled.div`
  display: flex;
  height: 100%;
  flex-direction: column;
  & > div {
    display: flex;
    flex: 1 1 auto;
    flex-direction: column;
    overflow-x: hidden;
    overflow-y: auto;
    background-color: #19212b;
  }
`;

const ChevronLeftIcon = styled(ChevronLeft)`
  color: rgba(255, 255, 255, 0.7);
  transform: rotate(0deg);
  transition: ${({ theme }) =>
    theme.transitions.create("transform", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })};

  ${({ open }) =>
    !open &&
    css`
      transform: rotate(180deg);
    `};
`;

const ButtonDrawer = styled.button.attrs({
  style: props => ({
    height: props.height || "48px"
  })
})`
  padding: 5px 15px;
  white-space: nowrap;
  display: flex;
  align-items: center;
  border: none;
  ${props =>
    props.borderTop
      ? css`
          border-top: 1px solid #404854;
          justify-content: flex-end;
        `
      : css`
          border-bottom: 1px solid #404854;

          justify-content: flex-start;
        `};
  text-align: ${props => (props.alignRight ? "right" : "left")};
  flex: 0 0 auto;
  background-color: transparent;
  &:focus {
    outline: none;
  }
  &:hover {
    ${ChevronLeftIcon} {
      color: white;
    }
  }
`;

const ListStyled = styled(List)`
  && {
    padding-top: 0px;
  }
`;
export {
  DrawerStyled,
  DrawerContainer,
  ButtonDrawer,
  ChevronLeftIcon,
  ListStyled
};
