import React from "react";
import { withRouter } from "react-router-dom";
import {
  ListItemStyled,
  ListItemTextStyled,
  IconStyled
} from "./ListItem.styled";
import { ListItemIcon } from "@material-ui/core";

export default withRouter(({ label, path, icon, location, history }) => {
  const selected = location.pathname === path;
  return (
    <ListItemStyled
      button
      onClick={() => history.push(path)}
      selected={selected}
    >
      <ListItemIcon>
        <IconStyled selected={selected}>{icon}</IconStyled>
      </ListItemIcon>
      <ListItemTextStyled selected={selected} primary={label} />
    </ListItemStyled>
  );
});
