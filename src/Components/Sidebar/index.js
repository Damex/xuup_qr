import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import {
  DrawerStyled,
  DrawerContainer,
  ButtonDrawer,
  ChevronLeftIcon,
  ListStyled
} from "./Sidebar.styled";
import ListItemStyled from "./ListItem";

const array = [
  { path: "/", label: "Home", icon: "home" },
  { path: "/store", label: "Tienda", icon: "store" },
  { path: "/products", label: "Productos", icon: "shopping_basket" },
  { path: "/orders", label: "Ventas", icon: "local_atm" }
];

class Sidebar extends Component {
  state = {
    open: false,
    path: ""
  };

  handleToggleDrawer = () => {
    this.setState({ open: !this.state.open });
  };
  render() {
    const { theme } = this.props;

    return (
      <DrawerStyled variant="permanent" open={this.state.open} theme={theme}>
        <DrawerContainer>
          <ButtonDrawer>
            <img
              alt="Logo Xuup"
              height={30}
              src={require("../../assets/logo_circle_plukke.png")}
            />
            <span
              style={{
                color: "white",
                fontSize: "1.5rem",
                paddingLeft: "1rem"
              }}
            >
              Xuup
            </span>
          </ButtonDrawer>
          <div>
            <ListStyled component="nav">
              {array.map(item => (
                <ListItemStyled key={item.path} {...item} />
              ))}
            </ListStyled>
          </div>
          <ButtonDrawer
            height="42px"
            borderTop
            alignRight
            onClick={this.handleToggleDrawer}
          >
            <ChevronLeftIcon open={this.state.open} theme={theme} />
          </ButtonDrawer>
        </DrawerContainer>
      </DrawerStyled>
    );
  }
}

export default withStyles(null, { withTheme: true })(Sidebar);
