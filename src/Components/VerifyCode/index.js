import React, { Component } from "react";
import { withRouter, Link } from "react-router-dom";
import qs from "query-string";
import _ from "lodash";
import { Auth } from "aws-amplify";
import { validateVerificationForm as validateFields } from "./validations";
import { getField, isEmptyObject } from "../../Utils";
import { cognitoErrors } from "../../Utils/Errors";
import { verificationForm } from "./utils";

class VerifyCode extends Component {
  state = {
    formValues: {},
    errors: {},
    loading: false,
    renderVerifyCode: false
  };
  resendCode = () => {
    const { username } = this.props;
    this.setState({ loading: true }, () => {
      Auth.resendSignUp(username)
        .then(() => console.log("Codigo enviado con exito!"))
        .catch(err => this.handleCodeErrors(err));
    });
  };

  isFormCompleted = formFields => {
    const { attributes } = this.props;
    const { formValues, errors } = this.state;
    let hasChange = false;

    if (!formFields.length) {
      return true;
    }

    if (attributes && !isEmptyObject(attributes)) {
      hasChange = _.reduce(
        formFields,
        (sum, n) =>
          sum ||
          (formValues[n.name] && formValues[n.name].trim()) !==
            attributes[n.name],
        false
      );
    } else {
      hasChange = true;
    }

    const requiredStepFields = _.filter(formFields, "required");
    const allHasValue = _.reduce(
      requiredStepFields,
      (sum, n) => sum && !!(formValues[n.name] && formValues[n.name].length),
      true
    );

    return allHasValue && isEmptyObject(errors) && hasChange;
  };

  onSubmit = e => {
    e.preventDefault();
    const { code } = this.state.formValues;
    const { username } = this.props;

    this.setState({ loading: true }, () => {
      Auth.confirmSignUp(username, code)
        .then(data => this.props.history.push("/join"))
        .catch(err => this.handleCodeErrors(err));
    });
  };

  handleCodeErrors = error => {
    this.setState({ loading: false });
    const { message } = cognitoErrors(error);
    /// Mostrar mensaje
    console.log(message);
  };

  setFormValue = (value, field) => {
    const { errors } = this.state;
    this.setState(
      prevState => ({
        formValues: {
          ...prevState.formValues,
          ...{ [field]: value }
        }
      }),
      () => {
        const formErrors = validateFields(field, value, errors);
        this.setState({ errors: formErrors });
      }
    );
  };

  setFormErrors = formErrors => {
    this.setState(() => ({ errors: formErrors }));
  };

  render() {
    const { formValues, errors } = this.state;
    const isEnabled = this.isFormCompleted(verificationForm.fields);

    return (
      <div>
        <form onSubmit={this.onSubmit}>
          {verificationForm.fields.map(field =>
            getField({
              ...field,
              key: field.name,
              name: field.name,
              onChange: this.setFormValue,
              setFormErrors: this.setFormErrors,
              value: formValues[field.name],
              errors
            })
          )}
          <button type="submit" disabled={!isEnabled}>
            Enviar
          </button>
          <Link
            to={{
              pathname: "/join",
              search: qs.stringify({ step: "sign-in" })
            }}
          >
            Iniciar sesión
          </Link>
        </form>
        <button onClick={this.resendCode}>Reenviar código</button>
      </div>
    );
  }
}

export default withRouter(VerifyCode);
