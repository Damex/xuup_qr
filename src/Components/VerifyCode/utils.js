export const verificationForm = {
  title: "Verificar",
  fields: [
    {
      type: "string",
      name: "code",
      label: "Código de verificación",
      help: "Ingresa el código de verificación",
      placeholder: "Código de verificación",
      required: true,
      validation: [],
      options: []
    }
  ]
};
