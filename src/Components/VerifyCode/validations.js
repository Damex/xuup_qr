import _ from "lodash";

export const validateVerificationForm = (fieldName, value, errors) => {
  if (fieldName === "code") {
    if (!value || !value.length) {
      _.assign(errors, {
        [fieldName]: "¡Requerido!"
      });
    } else if (value && value.length && !/^\d{6}$/.test(value)) {
      _.assign(errors, {
        [fieldName]: "¡El código debe ser de 6 dígitos!"
      });
    } else {
      delete errors[fieldName];
    }
  }

  return errors;
};
