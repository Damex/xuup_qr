import React from "react";
import { Cache } from "aws-amplify";
import _ from "lodash";

class UserData extends React.Component {
  first = true;
  componentDidUpdate() {
    const { loading, user, profile } = this.props;
    if (!loading && user && !_.isEmpty(profile)) {
      const { userPoolId } = user.pool;
      const { id, username } = profile;
      const userIID = Cache.getItem(`${username}_${userPoolId}_username`);
      if (this.first && _.isEmpty(userIID)) {
        this.first = false;
        this.props
          .mutate({
            variables: {
              username
            }
          })
          .then(res =>
            Cache.setItem(
              `${username}_${userPoolId}_username`,
              res.data.createUser.PK
            )
          )
          .catch(err => {
            console.log(err);
            if (id) {
              Cache.setItem(`${username}_${userPoolId}_username`, id);
            }
          });
      }
    }
  }

  render() {
    return null;
  }
}

export default UserData;
