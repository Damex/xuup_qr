import React from "react";
import numeral from "numeral";
import _ from "lodash";
import FormFields from "./FormFields";
import { getCombinations, fieldName } from "./utils";
import { Button } from "../Elements/Common";

export default class VariantsForm extends React.Component {
  state = {
    loading: true
  };

  componentDidMount() {
    const { currentStep, formValues, setFormValue } = this.props;
    const attributes = formValues[`${currentStep - 1}_attributes`];

    if (attributes) {
      const variants = Object.values(attributes);
      const combinations =
        variants &&
        variants.length &&
        getCombinations(variants, currentStep - 1);
      combinations &&
        combinations.length &&
        setFormValue(combinations, "combinations");
    }

    this.setState({ loading: false });
  }

  handleCustomIsStepCompleted = () => {
    const { stepContent, formValues, currentStep } = this.props;
    const stepFields =
      stepContent.fields &&
      stepContent.fields.length &&
      stepContent.fields[0].fields;
    const stepValues =
      formValues[`${currentStep}_variants`] &&
      Object.values(formValues[`${currentStep}_variants`]);

    const allHasValue = _.map(stepValues, value =>
      _.reduce(
        _.filter(stepFields, "required"),
        (sum, n) =>
          sum &&
          !!(
            value[fieldName(n, currentStep)] &&
            value[fieldName(n, currentStep)].length
          ),
        true
      )
    );
    const res =
      !stepContent.fields[0].min ||
      (!!stepValues && _.reduce(allHasValue, (acum, n) => acum && n, true));
    return res;
  };

  handlePrev = () => {
    const {
      prev,
      setFormValue,
      currentStep,
      errors,
      setFormErrors
    } = this.props;
    const errorKeys = Object.keys(errors);
    const r = window.confirm(
      "¿Estás seguro de regresar?, se perderán los valores ingresados en las variantes generadas."
    );

    if (r) {
      setFormValue(null, `${currentStep}_variants`);
      setFormValue([], `combinations`);

      if (errorKeys && errorKeys.length) {
        _.forEach(
          _.filter(errorKeys, e => {
            return numeral(e.split("_")[1]).value() === currentStep;
          }),
          f => delete errors[f]
        );
        setFormErrors(errors);
      }

      prev();
    }
  };

  render() {
    const {
      stepContent,
      formValues,
      currentStep,
      defaultButtons,
      customButtons = false,
      isLastStep,
      isStepCompleted,
      submit
    } = this.props;
    const { loading } = this.state;
    const customStepCompleted = this.handleCustomIsStepCompleted();
    return (
      <div>
        {stepContent.title}
        {loading ? (
          <div>Generando ...</div>
        ) : (
          <FormFields
            fields={stepContent.fields}
            {...this.props}
            combinations={formValues && formValues.combinations}
          />
        )}

        {!customButtons ? (
          defaultButtons
        ) : (
          <div>
            <Button onClick={this.handlePrev} disabled={!currentStep}>
              Anterior
            </Button>
            <Button
              onClick={submit}
              disabled={!isStepCompleted || !customStepCompleted}
            >
              {isLastStep ? "Guardar y Enviar" : "Siguiente"}
            </Button>
          </div>
        )}
      </div>
    );
  }
}
