import React from "react";
import FormFields from "./FormFields";

export default class GeneralsForm extends React.Component {
  render() {
    const { stepContent, defaultButtons, customButtons = false } = this.props;
    return (
      <div>
        {stepContent.title}
        <FormFields fields={stepContent.fields} {...this.props} />
        {!customButtons && defaultButtons}
      </div>
    );
  }
}
