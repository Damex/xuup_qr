import React from "react";
import _ from "lodash";
import FormFields from "./FormFields";
import { Button } from "../Elements/Common";
import { fieldName } from "./utils";

export default class CombinationsForm extends React.Component {
  handleCustomIsStepCompleted = () => {
    const { stepContent, formValues, currentStep } = this.props;
    const stepFields =
      stepContent.fields &&
      stepContent.fields.length &&
      stepContent.fields[0].fields;
    const stepValues =
      formValues[`${currentStep}_attributes`] &&
      Object.values(formValues[`${currentStep}_attributes`]);

    const allHasValue = _.map(stepValues, value =>
      _.reduce(
        _.filter(stepFields, "required"),
        (sum, n) =>
          sum &&
          !!(
            value[fieldName(n, currentStep)] &&
            value[fieldName(n, currentStep)].length
          ),
        true
      )
    );
    const res =
      !stepContent.fields[0].min ||
      (!!stepValues && _.reduce(allHasValue, (acum, n) => acum && n, true));
    return res;
  };

  render() {
    const {
      stepContent,
      defaultButtons,
      customButtons = false,
      prev,
      next,
      isLastStep,
      isStepCompleted,
      currentStep
    } = this.props;
    const customStepCompleted = this.handleCustomIsStepCompleted();

    return (
      <div>
        {stepContent.title}
        <FormFields fields={stepContent.fields} {...this.props} />

        {!customButtons ? (
          defaultButtons
        ) : (
          <div>
            <Button onClick={prev} disabled={!currentStep}>
              Anterior
            </Button>
            <Button
              onClick={next}
              disabled={!isStepCompleted || !customStepCompleted}
            >
              {isLastStep ? "Guardar y Enviar" : "Siguiente"}
            </Button>
          </div>
        )}
      </div>
    );
  }
}
