import React from "react";
import { withRouter } from "react-router-dom";
import numeral from "numeral";
import _ from "lodash";
import async from "async";
import { Mutation } from "react-apollo";
import { Storage } from "aws-amplify";

import {
  productForm,
  fieldName,
  isEmptyObject,
  parseFilesData,
  parseFormKeys,
  extractFiles
} from "./utils";
import Steps from "../Modules/Steps";
import GeneralsForm from "./GeneralsForm";
import CombinationsForm from "./CombinationsForm";
import VariantsForm from "./VariantsForm";
import validateFields from "./validations";

import CreateProduct from "../../GraphQL/Mutation/CreateProduct";

class NewProduct extends React.Component {
  state = {
    formValues: {},
    errors: {}
  };

  componentDidUpdate = () => {
    // Prevenir refrescar la página si el formulario tiene valor
    if (!isEmptyObject(this.state.formValues)) {
      window.onbeforeunload = () => true;
    }
  };

  // Función para comprobar estatus del paso actual
  isCurrentStepCompleted = (stepFields, step) => {
    const { formValues, errors } = this.state;

    if (!stepFields.length) {
      return true;
    }

    const requiredStepFields = _.filter(stepFields, "required");
    const allHasValue = _.reduce(
      requiredStepFields,
      (sum, n) =>
        sum &&
        !!(
          formValues[fieldName(n, step)] &&
          formValues[fieldName(n, step)].length
        ),
      true
    );

    const stepErrors = {};
    Object.keys(errors).forEach((errorKey, i) => {
      const ekp = _.split(errorKey, "_");
      const errorStep = ekp[ekp.length - 2];
      if (numeral(errorStep).value() === step) {
        _.assign(stepErrors, { [errorKey]: Object.values(errors)[i] });
      }
    });

    return allHasValue && isEmptyObject(stepErrors);
  };

  // Función para establecer valor de un campo
  setFormValue = (value, field) => {
    const { errors } = this.state;
    this.setState(
      prevState => ({
        formValues: {
          ...prevState.formValues,
          ...{ [field]: value }
        }
      }),
      () => {
        const formErrors = validateFields(
          field,
          value,
          errors,
          this.currentStep
        );
        this.setState({ errors: formErrors });
      }
    );
  };

  // Función para forzar establecer errores
  setFormErrors = formErrors => {
    this.setState(() => ({ errors: formErrors }));
  };

  // Función para guardar archivo
  // fileName = "manzana_verde_IID.ext"
  // key = "products/IID/manzana.ext"
  // key = "variants/IID/manzana_verde_0.ext"
  handleFilesUpload = fileList => {
    async.map(
      fileList,
      (file, callback) => {
        Storage.put(`${file.id}/${file.key}`, file.file, {
          customPrefix: { public: `${file.prefix}/` },
          bucket: file.bucket
        })
          .then(result => {
            callback(null, { ...result });
          })
          .catch(err => {
            callback(err);
          });
      },
      (err, results) => {
        if (err) {
          console.log(err);
        } else {
          console.log("Archivos cargados correctamente!");
          console.log(results);
          this.setState(() => ({ formValues: {}, errors: {} }));
          window.onbeforeunload = () => null;
          this.props.history.push("/products");
        }
      }
    );
  };

  // Función para enviar el formulario de creación de producto
  handleSubmit = async createProduct => {
    const { formValues } = this.state;
    const PFV = await parseFilesData(formValues);
    const parsedFormValues = parseFormKeys(PFV);

    //New Product Request
    const productRequest = {
      shop: "shop1",
      args: JSON.stringify(parsedFormValues)
    };

    console.log(productRequest);

    try {
      // Mutate for new product
      const { data } = await createProduct({
        variables: { ...productRequest }
      });
      const CPResponse = JSON.parse(data.createProduct);

      // On success parse & extract pictures files to put in storage
      const TFV = parseFilesData(formValues, CPResponse);
      const fileList = extractFiles(TFV);
      // console.log("FF", fileList);
      // On success put pictures in storage
      this.handleFilesUpload(fileList);
      // On success clear formValues & redirect to products list
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    const steps = [
      props => <GeneralsForm {...props} />,
      props => <CombinationsForm {...props} customButtons />,
      props => <VariantsForm {...props} customButtons />
    ];
    return (
      <Mutation mutation={CreateProduct}>
        {(createProduct, { data, loading, error, called }) => {
          return (
            <Steps
              steps={steps}
              schema={productForm}
              getCurrentStep={step => (this.currentStep = step)}
              isCurrentStepCompleted={this.isCurrentStepCompleted}
              setFormValue={this.setFormValue}
              setFormErrors={this.setFormErrors}
              formValues={this.state.formValues}
              errors={this.state.errors}
              handleSubmit={() => this.handleSubmit(createProduct)}
            />
          );
        }}
      </Mutation>
    );
  }
}

export default withRouter(NewProduct);
