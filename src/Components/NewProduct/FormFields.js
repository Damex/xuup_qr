import React from "react";

import getField from "../../Utils/GetField";
import { fieldName } from "./utils";

class FormFields extends React.Component {
  renderField = field => {
    const {
      setFormValue,
      setFormErrors,
      formValues,
      errors,
      currentStep,
      combinations,
      element
    } = this.props;

    const getValue = !field.origin
      ? formValues[fieldName(field, currentStep)]
      : formValues[`${currentStep}_${field.origin[0]}`] &&
        formValues[`${currentStep}_${field.origin[0]}`][field.name];

    const getValueBase64 =
      field.type === "file" ||
      field.type === "img" ||
      field.type === "[file]" ||
      field.type === "[img]"
        ? formValues[`${currentStep}_${field.name}Base64`]
        : null;

    return getField({
      ...field,
      key: fieldName(field, currentStep),
      name: fieldName(field, currentStep),
      onChange: setFormValue,
      setFormErrors,
      value: formValues && getValue,
      valueBase64: formValues && getValueBase64,
      errors,
      combinations,
      currentStep,
      element
    });
  };

  render() {
    return <div>{this.props.fields.map(field => this.renderField(field))}</div>;
  }
}

export default FormFields;
