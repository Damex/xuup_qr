import _ from "lodash";

export const productForm = [
  {
    title: "Datos del producto",
    fields: [
      {
        type: "string",
        name: "title",
        label: "Nombre del producto",
        help: "Ingresa el nombre del producto",
        placeholder: "",
        required: true,
        validation: [],
        options: []
      },
      {
        type: "textarea",
        name: "description",
        label: "Descripción",
        help: "Ingresa la descripción del producto",
        placeholder: "",
        required: false,
        validation: [],
        options: []
      },
      {
        type: "[string]",
        name: "tags",
        label: "Etiquetas",
        help: "Ingresa palabras clave del producto",
        placeholder: "",
        required: false,
        validation: [],
        options: []
      },
      {
        type: "img",
        name: "picture",
        label: "Foto",
        help: "Ingresa una foto principal del producto",
        placeholder: "",
        required: false,
        validation: [],
        options: []
      },
      {
        type: "select",
        name: "department",
        label: "Departamento",
        help: "Selecciona el dapartamento del producto",
        placeholder: "",
        required: false,
        validation: [],
        options: []
      },
      {
        type: "select",
        name: "category",
        label: "Categoría",
        help: "Selecciona la categoría del producto",
        placeholder: "",
        required: false,
        validation: [],
        options: []
      },
      {
        type: "string",
        name: "productCode",
        label: "Código del producto",
        help: "Ingresa el código del SAT del producto",
        placeholder: "",
        required: false,
        validation: [],
        options: []
      }
    ]
  },
  {
    title: "Generar variantes",
    fields: [
      {
        type: "array",
        name: "attributes",
        min: 0,
        max: 5,
        fields: [
          {
            type: "string",
            name: "variant",
            label: "Variante",
            placeholder: "tamaño, color, talla, etc",
            required: true
          },
          {
            type: "[string]",
            name: "options",
            label: "Opciones",
            placeholder: "chico, mediano, grande, etc",
            required: true
          }
        ]
      }
    ]
  },
  {
    title: "Datos de las variantes",
    fields: [
      {
        type: "array",
        name: "variants",
        min: 1,
        max: null,
        fields: [
          {
            type: "string",
            name: "title",
            label: "Variante",
            help: "Nombre de la variante",
            placeholder: "",
            required: false,
            validation: [],
            options: []
          },
          {
            type: "float",
            name: "price",
            label: "Precio de venta",
            help: "Ingresa el precio de venta incluyendo impuestos",
            placeholder: "",
            required: true,
            validation: [],
            options: []
          },
          {
            type: "float",
            name: "taxes",
            label: "Impuestos",
            help: "Ingresa el % de impuestos (0 - 100)",
            placeholder: "",
            required: true,
            validation: [],
            options: []
          },
          {
            type: "textarea",
            name: "description",
            label: "Descripción",
            help: "Descripción de la variante",
            placeholder: "",
            required: false,
            validation: [],
            options: []
          },
          {
            type: "[img]",
            name: "picture",
            label: "Fotos",
            help: "Ingresa fotos del producto",
            placeholder: "",
            required: false,
            validation: [],
            options: []
          },
          {
            type: "string",
            name: "barcode",
            label: "Código de barras",
            help: "Ingresa el código de barras",
            placeholder: "",
            required: false,
            validation: [],
            options: []
          },
          {
            type: "string",
            name: "sku",
            label: "SKU",
            help: "Ingresa el SKU del producto",
            placeholder: "",
            required: false,
            validation: [],
            options: []
          },
          {
            type: "float",
            name: "cost",
            label: "Costo",
            help: "Ingresa el costo",
            placeholder: "",
            required: false,
            validation: [],
            options: []
          },
          {
            type: "float",
            name: "initialInventory",
            label: "Inventario Inicial",
            help: "Ingresa el inventario inicial",
            placeholder: "",
            required: false,
            validation: [],
            options: []
          },
          {
            type: "integer",
            name: "minOrderQty",
            label: "Orden mínima",
            help: "Ingresa la cantidad mínima para pedidos",
            placeholder: "",
            required: false,
            validation: [],
            options: []
          }
        ]
      }
    ]
  }
];

export const fieldName = (field, step = 0) => `${step}_${field.name}`;

function cartesian(args) {
  const r = [];
  const arg = args;
  const max = arg.length - 1;
  function helper(arr, i) {
    for (let j = 0, l = arg[i].length; j < l; j++) {
      const a = arr.slice(0);
      a.push(arg[i][j]);
      if (i === max) {
        r.push(a);
      } else {
        helper(a, i + 1);
      }
    }
  }
  helper([], 0);
  return r;
}

function variantsAsObjects(variants, step) {
  return _.map(variants, variant =>
    _.map(variant[`${step}_options`], option => {
      const obj = {};
      obj[variant[`${step}_variant`]] = option;
      return obj;
    })
  );
}

export const isEmptyObject = obj => {
  return obj && Object.keys(obj).length === 0;
};

export const getCombinations = (variants, step = 0) =>
  _.map(cartesian(variantsAsObjects(variants, step)), opts => {
    const obj = {};
    _.forEach(opts, opt => {
      _.merge(obj, opt);
    });
    return obj;
  });

export const parseFilesData = (formValues, productResponse) => {
  const fv = Object.values(formValues);
  const newFormValues = formValues;
  Object.keys(formValues).forEach((fk, i) => {
    if (!_.startsWith(fk, "S3") && _.endsWith(fk, "picture")) {
      const files = fv[i];
      files &&
        files.length &&
        _.set(
          newFormValues,
          "S3_picture",
          files.map((x, idx) => {
            const t = {
              fileName: _.snakeCase(formValues["0_title"]),
              fileType: {
                oname: x.name,
                type: x.type,
                ext: x.name.split(".").pop()
              },
              prefix: "products"
            };

            if (productResponse && productResponse.product) {
              Object.assign(
                t,
                { file: x },
                productResponse.product.picture[idx],
                { id: productResponse.product.SK }
              );
            }

            return t;
          })
        );
    }

    if (_.endsWith(fk, "variants")) {
      const variants = Object.values(formValues)[i];
      const variantsValues = Object.values(variants);

      Object.keys(variants).forEach((vk, idx) => {
        Object.keys(variantsValues[idx]).forEach((vvk, index) => {
          if (!_.startsWith(vvk, "S3") && _.endsWith(vvk, "picture")) {
            const files = Object.values(variantsValues[idx])[index];
            files &&
              files.length &&
              _.set(
                newFormValues,
                `[${fk}][${vk}].S3_picture`,
                files.map((x, ix) => {
                  const t = {
                    fileName: `${_.snakeCase(
                      formValues["0_title"]
                    )}_${_.snakeCase(formValues[fk][vk]["2_title"])}_${ix}`,
                    fileType: {
                      oname: x.name,
                      type: x.type,
                      ext: x.name.split(".").pop()
                    },
                    prefix: "variants"
                  };

                  if (productResponse && productResponse.variants) {
                    Object.assign(
                      t,
                      { file: x },
                      productResponse.variants[idx].picture[ix],
                      { id: productResponse.variants[idx].SK }
                    );
                  }

                  return t;
                })
              );
          }
        });
      });
    }
  });
  return newFormValues;
};

export const parseFormKeys = formValues => {
  const fv = Object.values(formValues);
  const newFormValues = {};
  Object.keys(formValues).forEach((fk, i) => {
    if (_.endsWith(fk, "variants") || _.endsWith(fk, "attributes")) {
      const variants = Object.values(formValues)[i];
      const variantsValues = Object.values(variants);
      Object.keys(variants).forEach((vk, idx) => {
        Object.keys(variantsValues[idx]).forEach((vvk, index) => {
          if (_.startsWith(vvk, "S3") || !_.endsWith(vvk, "picture")) {
            _.set(
              newFormValues,
              `[${fk.split("_").pop()}][${vk.split("_").pop()}][${vvk
                .split("_")
                .pop()}]`,
              Object.values(variantsValues[idx])[index]
            );
          }
        });
      });
    } else {
      if (_.startsWith(fk, "S3") || !_.endsWith(fk, "picture"))
        Object.assign(newFormValues, { [fk.split("_").pop()]: fv[i] });
    }
  });
  return newFormValues;
};

export const extractFiles = formValues => {
  const fv = Object.values(formValues);
  const tf = [];
  Object.keys(formValues).forEach((fk, i) => {
    if (fk === "S3_picture") {
      const files = fv[i];
      files.forEach(file => tf.push(file));
    }

    if (_.endsWith(fk, "variants")) {
      const variants = Object.values(formValues)[i];
      const variantsValues = Object.values(variants);

      Object.keys(variants).forEach((vk, idx) => {
        Object.keys(variantsValues[idx]).forEach((vvk, index) => {
          if (vvk === "S3_picture") {
            const files = Object.values(variantsValues[idx])[index];
            files.forEach(file => tf.push(file));
          }
        });
      });
    }
  });

  return tf;
};
