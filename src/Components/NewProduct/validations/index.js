import _ from "lodash";
import numeral from "numeral";
import { isEmptyObject } from "../utils";

const validateFields = (fieldName, value, errors, step) => {
  if (_.endsWith(fieldName, "title")) {
    if (value.length < 4) {
      _.assign(errors, {
        [fieldName]: "Requerido, de al menos 3 caracteres"
      });
    } else {
      delete errors[fieldName];
    }
  }

  if (_.endsWith(fieldName, "productCode")) {
    if (value.length && !new RegExp("^[0-9]{8}$").test(value)) {
      _.assign(errors, { [fieldName]: "Debe ser de 8 digitos, solo números" });
    } else {
      delete errors[fieldName];
    }
  }

  if (_.endsWith(fieldName, "attributes")) {
    if (!isEmptyObject(value)) {
      Object.values(value).forEach((f, i) => {
        const element = Object.keys(value)[i];

        if (f[`${step}_variant`].length && !f[`${step}_options`].length) {
          _.assign(errors, {
            [`${element}_${step}_options`]: "Si el campo variante tiene valor, al menos debes ingresar una opción"
          });
        } else {
          delete errors[`${element}_${step}_options`];
        }

        if (f[`${step}_options`].length && !f[`${step}_variant`].length) {
          _.assign(errors, {
            [`${element}_${step}_variant`]: "Si el campo opción tiene valor, debes ingresar valor a variante"
          });
        } else {
          delete errors[`${element}_${step}_variant`];
        }
      });
    }
  }

  if (_.endsWith(fieldName, "variants")) {
    if (value && !isEmptyObject(value)) {
      Object.values(value).forEach((f, i) => {
        const element = Object.keys(value)[i];

        if (!f[`${step}_price`].length) {
          _.assign(errors, {
            [`${element}_${step}_price`]: "Precio es requerido"
          });
        } else {
          delete errors[`${element}_${step}_price`];
        }

        if (
          !f[`${step}_taxes`].length ||
          !_.inRange(numeral(f[`${step}_taxes`]).value(), 0, 100.01)
        ) {
          _.assign(errors, {
            [`${element}_${step}_taxes`]: "Impuestos es requerido, 0.00-100.00"
          });
        } else {
          delete errors[`${element}_${step}_taxes`];
        }
      });
    }
  }

  return errors;
};

export default validateFields;
