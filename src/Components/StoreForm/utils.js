export const shopForm = {
  title: "Crea tu tienda al alcance de un scan.",
  fields: [
    {
      type: "string",
      name: "alias",
      label: "Alias",
      help: "Alias de tu cuenta bancaria o tarjeta de credito",
      placeholder: "",
      required: false,
      validation: [],
      options: []
    },
    {
      type: "select",
      name: "type",
      label: "Tipo de cuenta",
      help: "",
      placeholder: "",
      required: true,
      validation: [],
      options: [
        { value: "TD", label: "Tarjeta de débito" },
        { value: "TC", label: "Tarjeta de crédito" },
        { value: "CL", label: "CLABE interbancaria" }
      ]
    },
    {
      type: "string",
      name: "cl",
      label: "Cuenta CLABE/tarjeta",
      help: "18 dígitos cuenta CLABE / 16 dígitos tarjeta debito/credito",
      placeholder: "",
      required: true,
      validation: [],
      options: []
    },
    {
      type: "string",
      name: "refa",
      label: "Nombre del beneficiaro",
      help: "Nombre completo del titular de la cuenta.",
      placeholder: "",
      required: true,
      validation: [],
      options: []
    },
    {
      type: "select",
      name: "bank",
      label: "Clave del banco",
      help: "Elige el banco al que pertenece la cuenta",
      placeholder: "",
      required: true,
      validation: [],
      options: [
        { value: "00002", label: "BANAMEX" },
        { value: "00012", label: "BBVA BANCOMER" },
        { value: "00014", label: "SANTANDER" },
        { value: "00021", label: "HSBC" },
        { value: "00036", label: "INBURSA" },
        { value: "00127", label: "AZTECA" }
      ]
    },
    {
      type: "select",
      name: "country",
      label: "País",
      help: "",
      placeholder: "",
      required: true,
      validation: [],
      options: [{ value: "MX", label: "México" }]
    },
    {
      type: "select",
      name: "currency",
      label: "Divisa",
      help: "",
      placeholder: "",
      required: true,
      validation: [],
      options: [{ value: "MXN", label: "MXN" }]
    }
  ]
};

export const parseValue = (value, field) => {
  switch (field) {
    case "cl":
      return value.replace(/\D+/g, "").substring(0, 18);
    default:
      return value;
  }
};
