import React, { Component, Fragment } from "react";
import { withRouter } from "react-router-dom";
import { Paper } from "@material-ui/core";
import _ from "lodash";
import { shopForm, parseValue } from "./utils";
import { validateStoreForm as validateFields } from "./validations";
import { getField, isEmptyObject } from "../../Utils";
import { Button } from "../Elements/Button.styled";
import { Mutation } from "../../../node_modules/react-apollo";
import CreateShop from "../../GraphQL/Mutation/CreateShop";

class StoreForm extends Component {
  state = {
    formValues: {},
    errors: {},
    loading: false
  };
  componentDidMount() {
    const { alias } = this.props;
    const formValues = { alias };
    this.setState({ formValues });
  }
  isFormCompleted = formFields => {
    const { attributes } = this.props;
    const { formValues, errors } = this.state;
    let hasChange = false;

    if (!formFields.length) {
      return true;
    }

    if (attributes && !isEmptyObject(attributes)) {
      hasChange = _.reduce(
        formFields,
        (sum, n) =>
          sum ||
          (formValues[n.name] && formValues[n.name].trim()) !==
            attributes[n.name],
        false
      );
    } else {
      hasChange = true;
    }

    const requiredStepFields = _.filter(formFields, "required");
    const allHasValue = _.reduce(
      requiredStepFields,
      (sum, n) => sum && !!(formValues[n.name] && formValues[n.name].length),
      true
    );

    return allHasValue && isEmptyObject(errors) && hasChange;
  };

  onSubmit(e, createShop) {
    e.preventDefault();
    const { email, alias: username } = this.props;
    const data = JSON.stringify({
      email,
      ...this.state.formValues,
      id: username
    });
    this.setState({ loading: true }, () => {
      createShop({ variables: { data } })
        .then(result => this.props.refetch())
        .catch(err => console.log(err));
    });
  }

  setFormValue = (value, field) => {
    const { errors } = this.state;
    this.setState(
      prevState => ({
        formValues: {
          ...prevState.formValues,
          ...{ [field]: parseValue(value, field) }
        }
      }),
      () => {
        const formErrors = validateFields(field, value, errors);
        this.setState({ errors: formErrors });
      }
    );
  };

  setFormErrors = formErrors => {
    this.setState(() => ({ errors: formErrors }));
  };

  render() {
    const { formValues, errors } = this.state;
    const isEnabled = this.isFormCompleted(shopForm.fields);

    return (
      <Mutation mutation={CreateShop}>
        {createShop => (
          <Fragment>
            <h2 style={{ textAlign: "center" }}>{shopForm.title}</h2>
            <Paper elevation={2}>
              <form
                onSubmit={event => this.onSubmit(event, createShop)}
                style={{ padding: "2rem" }}
              >
                {shopForm.fields.map(field =>
                  getField({
                    ...field,
                    key: field.name,
                    name: field.name,
                    onChange: this.setFormValue,
                    setFormErrors: this.setFormErrors,
                    value: formValues[field.name],
                    errors
                  })
                )}
                <div
                  style={{
                    display: "flex",
                    flexGrow: "grow",
                    justifyContent: "flex-end",
                    marginTop: "1rem"
                  }}
                >
                  <Button type="submit" disabled={!isEnabled}>
                    Crear tienda
                  </Button>
                </div>
              </form>
            </Paper>
          </Fragment>
        )}
      </Mutation>
    );
  }
}

export default withRouter(StoreForm);
