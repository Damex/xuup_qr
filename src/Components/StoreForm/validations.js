import _ from "lodash";

export const validateStoreForm = (fieldName, value, errors) => {
  switch (fieldName) {
    case "alias":
      break;
    default:
      if (!value || !value.length) {
        _.assign(errors, {
          [fieldName]: "¡Requerido!"
        });
      } else {
        delete errors[fieldName];
      }
  }

  return errors;
};
