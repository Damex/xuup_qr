export const ForgotPasswordForm = {
  title: "Bienvenido a Xuup",
  fields: [
    {
      type: "string",
      name: "username",
      label: "Nombre de tu tienda",
      help: "Este sera tu usuario",
      placeholder: "",
      required: true,
      validation: [],
      options: []
    }
  ]
};
