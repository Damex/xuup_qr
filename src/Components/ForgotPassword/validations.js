import _ from "lodash";

export const validateForgotForm = (fieldName, value, errors) => {
  if (fieldName === "username") {
    if (!value || !value.length) {
      _.assign(errors, {
        [fieldName]: "¡Requerido!"
      });
    } else {
      delete errors[fieldName];
    }
  }

  return errors;
};
