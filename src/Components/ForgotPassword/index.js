import React, { Component } from "react";
import _ from "lodash";
import { withRouter, Link } from "react-router-dom";
import qs from "query-string";
import { Auth } from "aws-amplify";
import { validateForgotForm as validateFields } from "./validations";
import { getField, isEmptyObject } from "../../Utils";
import { cognitoErrors } from "../../Utils/Errors";
import { ForgotPasswordForm } from "./utils";
import VerifyCodePassword from "../VerifyCodePassword";
import { ButtonStyled } from "../SignUp/SignUp.styled";

class ForgotPassword extends Component {
  state = {
    formValues: {},
    errors: {},
    loading: false,
    renderVerifyCode: false
  };

  onSubmit = e => {
    e.preventDefault();
    const { username } = this.state.formValues;

    this.setState({ loading: true }, () => {
      Auth.forgotPassword(username)
        .then(data => this.setState({ renderVerifyCode: true, loading: false }))
        .catch(err => this.handleErrors(err));
    });
  };
  isFormCompleted = formFields => {
    const { attributes } = this.props;
    const { formValues, errors } = this.state;
    let hasChange = false;

    if (!formFields.length) {
      return true;
    }

    if (attributes && !isEmptyObject(attributes)) {
      hasChange = _.reduce(
        formFields,
        (sum, n) =>
          sum ||
          (formValues[n.name] && formValues[n.name].trim()) !==
            attributes[n.name],
        false
      );
    } else {
      hasChange = true;
    }

    const requiredStepFields = _.filter(formFields, "required");
    const allHasValue = _.reduce(
      requiredStepFields,
      (sum, n) => sum && !!(formValues[n.name] && formValues[n.name].length),
      true
    );

    return allHasValue && isEmptyObject(errors) && hasChange;
  };
  handleErrors = error => {
    this.setState({ loading: false });
    console.log(error);
    const { message } = cognitoErrors(error);
    /// Mostrar mensaje
    console.log(message);
  };

  setFormValue = (value, field) => {
    const { errors } = this.state;
    this.setState(
      prevState => ({
        formValues: {
          ...prevState.formValues,
          ...{ [field]: value }
        }
      }),
      () => {
        const formErrors = validateFields(field, value, errors);
        this.setState({ errors: formErrors });
      }
    );
  };

  setFormErrors = formErrors => {
    this.setState(() => ({ errors: formErrors }));
  };

  render() {
    const { formValues, errors, renderVerifyCode } = this.state;
    const isEnabled = this.isFormCompleted(ForgotPasswordForm.fields);
    if (renderVerifyCode) {
      return <VerifyCodePassword username={formValues.username} />;
    }
    return (
      <div>
        <form
          onSubmit={this.onSubmit}
          style={{
            display: "flex",
            flexDirection: "column"
          }}
        >
          {ForgotPasswordForm.fields.map(field =>
            getField({
              ...field,
              key: field.name,
              name: field.name,
              onChange: this.setFormValue,
              setFormErrors: this.setFormErrors,
              value: formValues[field.name],
              errors
            })
          )}
          <ButtonStyled type="submit" disabled={!isEnabled}>
            Enviar
          </ButtonStyled>
          <Link
            to={{
              pathname: "/join",
              search: qs.stringify({ step: "forgot-password" })
            }}
            onClick={() =>
              this.setState({ renderVerifyCode: true, username: "" })
            }
          >
            Ingresar código para nueva contraseña
          </Link>
          <Link
            to={{
              pathname: "/join",
              search: qs.stringify({ step: "sign-in" })
            }}
          >
            Iniciar sesión
          </Link>
        </form>
      </div>
    );
  }
}

export default withRouter(ForgotPassword);
