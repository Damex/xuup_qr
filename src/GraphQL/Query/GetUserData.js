import gql from "graphql-tag";

export default gql`
  query GetUserData($PK: ID!) {
    getUserData(PK: $PK) {
      PK
      shop
      isOwner
    }
  }
`;
