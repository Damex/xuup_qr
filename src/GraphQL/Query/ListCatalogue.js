import gql from "graphql-tag";

export default gql`
  query listCatalogue($PK: ID!, $limit: Int, $nextToken: String) {
    listCatalogue(PK: $PK, limit: $limit, nextToken: $nextToken) {
      nextToken
      items {
        PK
        SK
        title
        description
        price
        product
        inventory
        productData {
          title
          slug
        }
      }
    }
  }
`;
