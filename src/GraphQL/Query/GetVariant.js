import gql from "graphql-tag";

export default gql`
  query getVariant($PK: ID!, $SK: ID!) {
    getVariant(PK: $PK, SK: $SK) {
      PK
      SK
      title
      description
      picture
      options
      price
      positions
      isDeleted
      isVisible
      isSoldOut
      createdAt
      updatedAt
      taxes
      cost
      barcode
      minOrderQty
      sku
      productData {
        title
      }
    }
  }
`;
