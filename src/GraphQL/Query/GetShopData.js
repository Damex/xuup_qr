import gql from "graphql-tag";

export default gql`
  query GetShopData($shopId: ID!, $username: String!) {
    getShopData(shopId: $shopId, username: $username)
  }
`;
