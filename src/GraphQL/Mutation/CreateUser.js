import gql from "graphql-tag";

export default gql`
  mutation CreateUser($username: String!) {
    createUser(username: $username) {
      PK
      isOwner
      shop
    }
  }
`;
