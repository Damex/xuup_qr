import gql from "graphql-tag";

export default gql`
  mutation CreateShop($data: AWSJSON) {
    createShop(data: $data)
  }
`;
