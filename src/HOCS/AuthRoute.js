import React from "react";
import { Redirect } from "react-router-dom";

import UserContext from "../Contexts/UserContext";

function authRoute(WrappedComponent) {
  return class AuthRoute extends React.Component {
    render() {
      return (
        <UserContext.Consumer>
          {({ signedIn, signOut, user, loading }) => {
            if (loading) {
              return <div>Cargando...</div>;
            }

            if (signedIn) {
              return <WrappedComponent {...this.props} />;
            }

            return (
              <Redirect
                to={{
                  pathname: "/join",
                  from: this.props.location
                }}
              />
            );
          }}
        </UserContext.Consumer>
      );
    }
  };
}

export default authRoute;
