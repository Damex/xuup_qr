import React, { Component } from "react";
import Amplify, { Auth, Hub, Logger } from "aws-amplify";
import { ApolloProvider, Mutation } from "react-apollo";
import AWSAppSyncClient from "aws-appsync/lib";
import { Rehydrated } from "aws-appsync-react/lib";
import { AUTH_TYPE } from "aws-appsync/lib/link/auth-link";

import AppSync from "../AppSync.js";
import UserContext from "../Contexts/UserContext.js";
import CreateUser from "../GraphQL/Mutation/CreateUser.js";
import UserData from "../Components/Users/UserData.js";
// import CreateUserRecord from "../GraphQL/Mutation/CreateUserRecord.js";
// import UserData from "../Components/Users/UserData.js";

// AMPLIFY Config
Amplify.configure({
  Analytics: {
    disabled: true
  },
  Auth: {
    identityPoolId: "us-east-1:8b307306-05d6-4b38-a87f-ce573cb42e93",
    region: "us-east-1",
    userPoolId: "us-east-1_nKutoNK28",
    userPoolWebClientId: "3v8rmu0p0sd3mep7eem0pm6ngq"
  },
  Storage: {
    bucket: "hkbbva-files", //REQUIRED -  Amazon S3 bucket
    region: "us-east-1" //OPTIONAL -  Amazon service region
  }
});

const client = new AWSAppSyncClient({
  disableOffline: true,
  url: AppSync.graphqlEndpoint,
  region: AppSync.region,
  auth: {
    type: AUTH_TYPE.API_KEY,
    apiKey: AppSync.apiKey
  }
});

// Amplify.Logger.LOG_LEVEL = 'DEBUG';
const logger = new Logger("WithProvider");

export default WrappedComponent => {
  return class WithProvider extends Component {
    constructor(props) {
      super(props);
      Hub.listen("auth", this, "AuthListener");

      this.signOut = () => {
        Auth.signOut()
          .then(() => {
            this.setState({ signedIn: false, user: null, profile: {} });
          })
          .catch(err => console.log(err));
      };

      this.handleShopId = (shopId, history) => {
        this.setState({ shopId }, () => history.push("/product/new"));
      };

      this.state = {
        loading: true,
        signedIn: false,
        profile: {},
        user: null,
        signOut: this.signOut,
        shopId: null,
        handleShopId: this.handleShopId
      };
    }

    async componentDidMount() {
      try {
        const user = await Auth.currentAuthenticatedUser();
        const profile = await Auth.currentUserInfo();
        this.setState({ signedIn: !!user, user, profile, loading: false });
      } catch (err) {
        this.setState({
          signedIn: false,
          profile: {},
          user: null,
          loading: false
        });
        console.log(err);
      }
    }

    onHubCapsule(capsule) {
      const { channel, payload } = capsule;
      if (channel === "auth") {
        this.onAuthEvent(payload);
      }
    }

    async onAuthEvent(payload) {
      const { event, data } = payload;
      switch (event) {
        case "signIn":
          logger.info("user signed in");
          this.setState({ loading: true });
          try {
            const user = await Auth.currentAuthenticatedUser();
            const profile = await Auth.currentUserInfo();
            this.setState({ signedIn: true, user, profile, loading: false });
          } catch (err) {
            console.log(err);
          }
          break;
        case "signUp":
          logger.info("user signed up");
          break;
        case "signOut":
          logger.info("user signed out");
          this.setState({ signedIn: false, user: null, profile: {} });
          break;
        case "signIn_failure":
          logger.info("user sign in failed");
          this.setState({ signedIn: false, user: null, profile: {} });
          break;
        case "attributes_change":
          logger.info("attributes updated");
          const profile = await Auth.currentUserInfo();
          this.setState({ profile });
          break;
        case "new_password_required":
          logger.info("User requires new password");
          this.setState({ user: data });
          break;
        default:
          logger.info("nothing happens");
      }
    }
    render() {
      return (
        <ApolloProvider client={client}>
          <Rehydrated>
            <UserContext.Provider value={this.state}>
              <WrappedComponent />
              <Mutation mutation={CreateUser}>
                {mutate => <UserData {...this.state} mutate={mutate} />}
              </Mutation>
            </UserContext.Provider>
          </Rehydrated>
        </ApolloProvider>
      );
    }
  };
};
