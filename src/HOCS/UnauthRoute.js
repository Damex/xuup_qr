import React from "react";
import { Redirect } from "react-router-dom";

import UserContext from "../Contexts/UserContext";

function unauthRoute(WrappedComponent) {
  return class UnauthRoute extends React.Component {
    render() {
      return (
        <UserContext.Consumer>
          {({ signedIn, signOut, user, loading }) => {
            if (loading) {
              return <div>Cargando...</div>;
            }

            if (!signedIn) {
              return <WrappedComponent {...this.props} />;
            }

            return <Redirect to="/" />;
          }}
        </UserContext.Consumer>
      );
    }
  };
}

export default unauthRoute;
